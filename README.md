
**ChicknicAndFrenys**: Esp8266 based, wearable station with PM 1/2.5/10 and TVOC sensors.

**MoburAndFrenys**: Esp8266 based, stationary station with PM 1/2.5/10, CO2 and VOC level sensors

**PumperAndFrenys**: Esp32 based, wearable station with PM 1/2.5/10 and TVOC sensors.
