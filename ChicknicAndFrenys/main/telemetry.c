/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2020 by Daniel Quintela
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files 
 * (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, 
 * publish, distribute, sublicense, and/or sell copies of the Software, 
 * and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be 
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY 
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/* 
 * File:   telemetry.c
 * Author: danielq
 *
 * Created on 11 de enero de 2020, 10:32
 */

#include "caf.h"

#include "esp_system.h"
#include <sys/time.h>

#include "bmp280.h"
#include "ccs811.h"

extern int idxSsid;

static const char *TAG = "telemetry";
static int k;
static volatile int state;
static struct timeval tv;
static struct timezone *tz;
static bool haveCnxn;
static SemaphoreHandle_t semWeather;
static SemaphoreHandle_t semI2c;
static struct HbbTelemetryEntry hteWeather;
static bmp280_t bme;
static ccs811_sensor_t *ccs;
static uint16_t difTvoc;
static int eqlsTvoc;
static time_t firstTvSec;

static void ccs811SetWeather(void) {
    xSemaphoreTake(semI2c, portMAX_DELAY);
    bool ok = ccs811_set_environmental_data(ccs, hteWeather.t, hteWeather.h);
    ESP_LOGI(TAG, "ccs811_set_environmental_data %d", ok);
    xSemaphoreGive(semI2c);
}

static void ccs811Read(void) {
    if(ccs != NULL) {
        xSemaphoreTake(semI2c, portMAX_DELAY);
        bool ok = ccs811_get_results(ccs, &hteWeather.tvoc, &hteWeather.co2, NULL, NULL);
        ESP_LOGI(TAG, "ccs811_get_results %d 0x%X", ok, ccs->error_code);
        xSemaphoreGive(semI2c);
        if(ok) {
            if(hteWeather.tvoc == difTvoc) {
                eqlsTvoc += 1;
            } else {
                difTvoc = hteWeather.tvoc;
            }
        }
        if(! ok || eqlsTvoc > 60) {
            ESP_LOGE(TAG, "ccs811 reinit");
            free(ccs);
            ccs = NULL;
            commsPublishMessage(HTE_MSG_CCS_REINIT);
        }
    }
}

static esp_err_t bme280Read(void) {
    float temperature, pressure, humidity = 0.0;
    xSemaphoreTake(semI2c, portMAX_DELAY);
    esp_err_t ee = bmp280_read_float(&bme, &temperature, &pressure, &humidity);
    xSemaphoreGive(semI2c);
    ESP_LOGI(TAG, "bmp280_read_float %d %f %f %f\n", ee, temperature, pressure, humidity);
    if(ee == ESP_OK) {
        hteWeather.h = humidity;
        hteWeather.p = pressure / 100.0;
        hteWeather.t = temperature;
    }
    return ee;
}

static esp_err_t bme280Init(void) {
    bmp280_params_t params;
    bmp280_init_default_params(&params);
    memset(&bme, 0, sizeof(bmp280_t));
    esp_err_t ee = bmp280_init_desc(&bme, BMP280_I2C_ADDRESS_0, I2C_MASTER_NUM, I2C_MASTER_SDA_IO, I2C_MASTER_SCL_IO);
    ESP_LOGI(TAG, "bmp280_init_desc %d\n", ee);
    if(ee == ESP_OK) {
        xSemaphoreTake(semI2c, portMAX_DELAY);
        ee = bmp280_init(&bme, &params);
        xSemaphoreGive(semI2c);
        ESP_LOGI(TAG, "bmp280_init %d\n", ee);
    }    
    return ee;
}



static void pmsClose(void) {
    esp_err_t ret = 0;
    gpio_set_level(PMS_SET_GPIO, 0);
    ESP_LOGI(TAG, "Close measurement %d", ret);
}

static void pmsOpen(void) {
    esp_err_t ret = 0;
    gpio_set_level(PMS_SET_GPIO, 1);
    ESP_LOGI(TAG, "Open measurement %d", ret);
}

static void pmsReadAndPublish(void) {
    uint8_t data[PMS_FRAME_SIZE];
    xSemaphoreTake(semI2c, portMAX_DELAY);
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (I2C_PMS_ADDRESS << 1) | I2C_MASTER_READ, 1);
    i2c_master_read(cmd, data, PMS_FRAME_SIZE, 0);
    i2c_master_stop(cmd);
    esp_err_t ret = i2c_master_cmd_begin(I2C_MASTER_NUM, cmd, 1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);
    xSemaphoreGive(semI2c);
    ESP_LOGI(TAG, "Read data %d", ret);
    if(ret == ESP_OK) {
        ESP_LOG_BUFFER_HEXDUMP(TAG, data, PMS_FRAME_SIZE, ESP_LOG_INFO);
    }
    // calculamos el check code
    uint16_t ccc = ((uint16_t) data[30]) << 8;
    ccc |= data[31];
    for(int i = 0, l = PMS_FRAME_SIZE - 2; i < l; i++) {
        ccc -= data[i];
    }
    ESP_LOGI(TAG, "ccc 0x%X", ccc);
    if(ccc == 0) {
        // el check code calculado es igual al recibido: extraemos los datos
        int t10 = ((int) data[4]) << 8;
        t10 |= data[5];
        int t25 = ((int) data[6]) << 8;
        t25 |= data[7];
        int t100 = ((int) data[8]) << 8;
        t100 |= data[9];
        ESP_LOGI(TAG, "TSI PM1.0 %d, PM2.5 %d, PM10 %d", 
                t10, t25, t100);
        gettimeofday(&tv, &tz);
        ESP_LOGI(TAG, "time %ld.%06ld", tv.tv_sec, tv.tv_usec);
        if(tv.tv_sec > 1500000000) {
            xSemaphoreTake(semWeather, portMAX_DELAY);
            struct HbbTelemetryEntry hte = {
                .flags = HTE_HAS_PM | (ccs == NULL ? 0 : HTE_HAS_TVOC),
                .ts = tv.tv_sec,
                .t = hteWeather.t,
                .h = hteWeather.h,
                .p = hteWeather.p,
                .co2 = hteWeather.co2,
                .tvoc = hteWeather.tvoc,
                .pm10 = t10,
                .pm25 = t25,
                .pm100 = t100
            };
            xSemaphoreGive(semWeather);
            ESP_LOGI(TAG, "CO2 %d, VOC %d, humedad %.1f, temperatura %.1f", 
                    hte.co2, hte.tvoc, hte.h, hte.t);
            hte.ts *= 1000;
            hte.ts += tv.tv_usec / 1000;
            if(setEntryPosition(&hte) == ESP_OK) {
                commsPublishData(&hte);
            }
        }
    } else {
        int t10 = 0;
        int t25 = 0;
        int t100 = 0;
        ESP_LOGI(TAG, "TSI PM1.0 %d, PM2.5 %d, PM10 %d", 
                t10, t25, t100);
        gettimeofday(&tv, &tz);
        ESP_LOGI(TAG, "time %ld.%06ld", tv.tv_sec, tv.tv_usec);
        if(tv.tv_sec > 1500000000) {
            xSemaphoreTake(semWeather, portMAX_DELAY);
            struct HbbTelemetryEntry hte = {
                .flags = 0 | (ccs == NULL ? 0 : HTE_HAS_TVOC),
                .ts = tv.tv_sec,
                .t = hteWeather.t,
                .h = hteWeather.h,
                .p = hteWeather.p,
                .co2 = hteWeather.co2,
                .tvoc = hteWeather.tvoc,
                .pm10 = t10,
                .pm25 = t25,
                .pm100 = t100
            };
            xSemaphoreGive(semWeather);
            ESP_LOGI(TAG, "CO2 %d, VOC %d, humedad %.1f, temperatura %.1f", 
                    hte.co2, hte.tvoc, hte.h, hte.t);
            hte.ts *= 1000;
            hte.ts += tv.tv_usec / 1000;
            if(setEntryPosition(&hte) == ESP_OK) {
                commsPublishData(&hte);
            }
        }
    }
    ESP_LOGI(TAG, "esp_get_free_heap_size 0x%X", esp_get_free_heap_size());
}

static void weatherTask(void* param) {
    TickType_t toBme = 10000 / portTICK_PERIOD_MS;
    TickType_t toCcs = 5000 / portTICK_PERIOD_MS;
    TickType_t toNxt = toBme;
    while(true) {
        vTaskDelay(toNxt);
        if(ccs == NULL) {
            difTvoc = 0xFFFF;
            eqlsTvoc = 0;
            ccs = ccs811_init_sensor(I2C_MASTER_NUM, CCS811_I2C_ADDRESS_1);
            ESP_LOGI(TAG, "ccs811_init_sensor 0x%X", (uint32_t) ccs);
        }
        if(state > 1) {
            xSemaphoreTake(semWeather, portMAX_DELAY);
            if(toNxt == toBme) {
                if(bme280Read() == ESP_OK) {
                    ccs811SetWeather();
                }
                toNxt = toCcs;
            } else {
                ccs811Read();
                toNxt = toBme;
            }
            xSemaphoreGive(semWeather);
        }
    }
}

esp_err_t telemetryInit(void) {
    semWeather = xSemaphoreCreateMutex();
    semI2c = xSemaphoreCreateMutex();

    gpio_set_direction(PMS_SET_GPIO, GPIO_MODE_OUTPUT);
    
    firstTvSec = 0;

    int i2c_master_port = I2C_MASTER_NUM;
    i2c_config_t conf;
    conf.mode = I2C_MODE_MASTER;
    conf.sda_io_num = I2C_MASTER_SDA_IO;
    conf.sda_pullup_en = GPIO_PULLUP_ENABLE;
    conf.scl_io_num = I2C_MASTER_SCL_IO;
    conf.scl_pullup_en = GPIO_PULLUP_ENABLE;
    conf.clk_stretch_tick = I2C_CLK_STRETCH_TICK;
    i2c_driver_install(i2c_master_port, conf.mode);
    i2c_param_config(i2c_master_port, &conf);
    uiInit(I2C_MASTER_NUM, semI2c);
    haveCnxn = false;
    state = 0;
    k = 0;
    esp_err_t ee = bme280Init();
    ESP_LOGI(TAG, "bme280Init 0x%X", ee);
    if(ee == ESP_OK) {
        ccs = NULL;
        xTaskCreate(&weatherTask, "weather_task", 2048, NULL, 5, NULL);
    }
    uiShow(CAF_STATE_BOOTING, ee);
    return ESP_OK;
}

void telemetryTask(void* param) {
    
    while(true) {
        uint32_t qi;
        if(xQueueReceive(qTelemetry, &qi, 1000 / portTICK_PERIOD_MS)) {
            ESP_LOGI(TAG, "telemetryTask qi 0x%X", qi);
            switch(qi >> 24) {
                case CAF_TELEMETRY_CMD:
                    switch((qi & 0xFF0000) >> 16) {
                        case CAF_TELEMETRY_CMD_DISC:
                            uiShow(CAF_STATE_NOT_CONNECTED, 0);
                            haveCnxn = false;
                            break;
                        case CAF_TELEMETRY_CMD_CONN:
                            uiShow(CAF_STATE_CONNECTED, idxSsid + 1);
                            haveCnxn = true;
                            break;
                    }
                    break;
                case CAF_TELEMETRY_MSG:
                    uiShow((qi & 0xFF0000) >> 16, qi & 0xFFFF);
                    break;
            }
        } else {
            k++;
            switch(state) {
                case 0:
                    pmsClose();
                    k = 0;
                    state += 2;
                    break;
                case 2:
                    if(k >= PMS_SECONDS_FOR_OPEN) {
                        pmsOpen();
                        state++;
                        uiShow(CAF_STATE_READING, 0);
                    }
                    break;
                case 3:
                    if(k >= PMS_SECONDS_FOR_READ) {
                        pmsReadAndPublish();
                        state = 0;
                        uiShow(CAF_STATE_IDLE, 0);
                    }
                    break;
            }            
        }        
    }
}

void telemetryTaskCmd(uint8_t cmd) {
    ESP_LOGI(TAG, "telemetryTaskCmd 0x%X", cmd);
    uint32_t qi = (CAF_TELEMETRY_CMD << 24) | ((cmd & 0xFF) << 16);
    xQueueSendToBack(qTelemetry, &qi, portMAX_DELAY);
}

void telemetryTaskMsg(uint8_t type, uint32_t data) {
    ESP_LOGI(TAG, "telemetryTaskMsg 0x%X 0x%X", type, data);
    uint32_t qi = (CAF_TELEMETRY_MSG << 24) | ((type & 0xFF) << 16) | (data & 0xFFFF);
    xQueueSendToBack(qTelemetry, &qi, portMAX_DELAY);
}
