/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2020 by Daniel Quintela
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files 
 * (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, 
 * publish, distribute, sublicense, and/or sell copies of the Software, 
 * and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be 
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY 
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/* 
 * File:   caf.h
 * Author: danielq
 *
 * Created on 13 de febrero de 2020, 09:57
 */

#ifndef CAF_H
#define CAF_H

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "driver/i2c.h"
#include "driver/uart.h"
#include <math.h>
#include <string.h>

#include <nvs_flash.h>
#include <esp_log.h>

#define I2C_MASTER_SCL_IO          GPIO_NUM_5         /*!< gpio number for I2C master clock */
#define I2C_MASTER_SDA_IO          GPIO_NUM_4         /*!< gpio number for I2C master data  */
#define I2C_MASTER_NUM             I2C_NUM_0          /*!< I2C port number for master dev */
#define I2C_CLK_STRETCH_TICK       300                /*!< 300 ticks, Clock stretch is about 210us, you can make changes according to the actual situation */
#define I2C_MASTER_TX_BUF_DISABLE  0                  /*!< I2C master doesn't need buffer */
#define I2C_MASTER_RX_BUF_DISABLE  0                  /*!< I2C master doesn't need buffer */
#define GPS_UART_NUM               UART_NUM_0
#define I2C_PMS_ADDRESS            0x12
#define PMS_SECONDS_FOR_OPEN       120
#define PMS_SECONDS_FOR_READ       150
#define PMS_SET_GPIO               GPIO_NUM_16
#define UI_RED_PIN                 GPIO_NUM_14
#define UI_GREEN_PIN               GPIO_NUM_12
#define UI_BLUE_PIN                GPIO_NUM_13
#define PMS_FRAME_SIZE             32

//#   define AERK_PROXY_URL "https://soongsoft.com/aerkproxy/telemetry"
#   define AERK_PROXY_URL "http://soongsoft.com/aerkproxy/telemetry"
//#   define AERK_PROXY_URL "https://11.0.0.244/aerkproxy/telemetry"

#define HTE_QUEUE_LENGTH 256
#define HTE_HAS_PM       1
#define HTE_HAS_TVOC     2
#define HTE_IS_A_MESSAGE 0x80

#define CAF_TELEMETRY_CMD           0
#define CAF_TELEMETRY_MSG           1

#define CAF_TELEMETRY_CMD_DISC      0
#define CAF_TELEMETRY_CMD_CONN      1

#define CAF_STATE_BOOTING           0
#define CAF_STATE_CONNECTING        1
#define CAF_STATE_NOT_CONNECTED     2
#define CAF_STATE_CONNECTED         3
#define CAF_STATE_IDLE              4
#define CAF_STATE_READING           5

#define HTE_MSG_CF_STARTING         0
#define HTE_MSG_CCS_REINIT          1

typedef enum {
    MQTT_OFF = 0,
    MQTT_ON
} telemetryCmds;

struct HbbTelemetryEntry {
    uint8_t flags;
    uint8_t iMsg;
    int64_t ts;
    float lat;
    float lon;
    float t;
    float h;
    float p;
    uint16_t pm10;
    uint16_t pm25;
    uint16_t pm100;
    uint16_t tvoc;
    uint16_t co2;
};


esp_err_t commsInit(void);
void commsTask(void* param);
esp_err_t commsPublishData(struct HbbTelemetryEntry * pHte);
esp_err_t commsPublishMessage(const uint8_t iMsg);

esp_err_t telemetryInit(void);
void telemetryTask(void* param);
void telemetryTaskCmd(uint8_t cmd);
void telemetryTaskMsg(uint8_t type, uint32_t data);

esp_err_t positionInit(void);
void positionTask(void* param);
esp_err_t setEntryPosition(struct HbbTelemetryEntry *pHte);

esp_err_t uiInit(i2c_port_t port, SemaphoreHandle_t sem);
esp_err_t uiShow(int state, esp_err_t error);

extern QueueHandle_t qTelemetry;
extern EventGroupHandle_t egComms;
    

#endif /* MAF_H */

