/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2020 by Daniel Quintela
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files 
 * (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, 
 * publish, distribute, sublicense, and/or sell copies of the Software, 
 * and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be 
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY 
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/* 
 * File:   main.c
 * Author: danielq
 *
 * Created on 13 de febrero de 2020, 11:27
 */

#include "caf.h"

QueueHandle_t qTelemetry;
EventGroupHandle_t egComms;

static int uart1PutChar(int ch) {
    char c = ch & 0xFF;
    
    uart_write_bytes(UART_NUM_1, &c, 1);
    
    return ch;
}

void app_main(void) {

    // logging to UART 1
    uart_config_t uart_config = {
        .baud_rate = 9600,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
    };
    uart_param_config(UART_NUM_1, &uart_config);
    uart_driver_install(UART_NUM_1, 256, 0, 0, NULL, 0);
    esp_log_set_putchar(&uart1PutChar);
    
    qTelemetry = xQueueCreate(10, sizeof(uint32_t));
    egComms =  xEventGroupCreate();
    
    ESP_ERROR_CHECK(nvs_flash_init());

    ESP_ERROR_CHECK(commsInit());
    ESP_ERROR_CHECK(telemetryInit());
    ESP_ERROR_CHECK(positionInit());
    
    xTaskCreate(&commsTask, "comms_task", 4096, NULL, 5, NULL);
    xTaskCreate(&telemetryTask, "telemetry_task", 8192, NULL, 5, NULL);
    xTaskCreate(&positionTask, "position_task", 2048, NULL, 7, NULL);    
    
}