/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2020 by Daniel Quintela
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files 
 * (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, 
 * publish, distribute, sublicense, and/or sell copies of the Software, 
 * and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be 
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY 
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/* 
 * File:   maf.h
 * Author: danielq
 *
 * Created on 11 de enero de 2020, 09:57
 */

#ifndef MAF_H
#define MAF_H

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include <math.h>

#include <nvs_flash.h>
#include <esp_log.h>

#define PUBLISH_VIA_AERK_PROXY

#define I2C_MASTER_SCL_IO GPIO_NUM_5                /*!< gpio number for I2C master clock */
#define I2C_MASTER_SDA_IO GPIO_NUM_4                /*!< gpio number for I2C master data  */
#define I2C_MASTER_NUM I2C_NUM_0                    /*!< I2C port number for master dev */
#define I2C_CLK_STRETCH_TICK 300                    /*!< 300 ticks, Clock stretch is about 210us, you can make changes according to the actual situation */
#define I2C_MASTER_TX_BUF_DISABLE 0                 /*!< I2C master doesn't need buffer */
#define I2C_MASTER_RX_BUF_DISABLE 0                 /*!< I2C master doesn't need buffer */
#define I2C_CUBIC_ADDRESS 0x28
#define I2C_CUBIC_FRAME_SIZE 25
#define CUBIC_SECONDS_FOR_OPEN 120
#define CUBIC_SECONDS_FOR_READ 150

#ifndef PUBLISH_VIA_AERK_PROXY
#   define AERK_MQTT_TOPIC "v1/devices/me/telemetry"
#else
//#   define AERK_PROXY_URL "https://soongsoft.com/aerkproxy/telemetry"
#   define AERK_PROXY_URL "http://soongsoft.com/aerkproxy/telemetry"
//#   define AERK_PROXY_URL "https://11.0.0.244/aerkproxy/telemetry"
#endif


esp_err_t commsInit(void);
void commsTask(void* param);
esp_err_t commsPublishData(char* data);

esp_err_t telemetryInit(void);
void telemetryTask(void* param);

extern EventGroupHandle_t egTelemetry;
extern EventGroupHandle_t egComms;
extern float fLati;
extern float fLong;
    

#endif /* MAF_H */

