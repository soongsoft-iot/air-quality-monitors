/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2020 by Daniel Quintela
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files 
 * (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, 
 * publish, distribute, sublicense, and/or sell copies of the Software, 
 * and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be 
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY 
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/* 
 * File:   main.c
 * Author: danielq
 *
 * Created on 10 de enero de 2020, 11:27
 */

#include "maf.h"

EventGroupHandle_t egTelemetry;
EventGroupHandle_t egComms;


void app_main(void) {
    
    egTelemetry =  xEventGroupCreate();
    egComms =  xEventGroupCreate();
    
    ESP_ERROR_CHECK(nvs_flash_init());

    ESP_ERROR_CHECK(commsInit());
    ESP_ERROR_CHECK(telemetryInit());
    
    xTaskCreate(&commsTask, "comms_task", 2048, NULL, 5, NULL);
    xTaskCreate(&telemetryTask, "telemetry_task", 8192, NULL, 5, NULL);
    
}