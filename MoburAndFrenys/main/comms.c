/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2020 by Daniel Quintela
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files 
 * (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, 
 * publish, distribute, sublicense, and/or sell copies of the Software, 
 * and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be 
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY 
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/* 
 * File:   comms.c
 * Author: danielq
 *
 * Created on 11 de enero de 2020, 10:06
 */

#include "maf.h"

#include <esp_wifi.h>
#include <esp_event_loop.h>
#include <esp_http_server.h>
#ifdef PUBLISH_VIA_AERK_PROXY
#   include <esp_http_client.h>
#else
#   include "mqtt_client.h"
#endif
#include "lwip/apps/sntp.h"


static const char *TAG = "comms";
#ifdef PUBLISH_VIA_AERK_PROXY
static const char* cfgForm =
"<!DOCTYPE HTML>"
"<html>"
"<head>"
"<meta charset=\"UTF-8\">"
"<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">"
"<title>New&nbsp;configuration</title>"
"</head>"
"<body>"
"<div style=\"border-style:solid;border-width:1px;border-color:black;padding-left:10px\">"
"<form action=\"\" method=\"POST\">"
"<br>"
"<div>"
"<label for=\"ssid\">SSID</label>"
"&nbsp;"
"<input type=\"text\" id=\"ssid\" name=\"ssid\" value=\"%s\">"
"</div>"
"<br>"
"<div>"
"<label for=\"pass\">Password</label>"
"&nbsp;"
"<input type=\"password\" id=\"pass\" name=\"pass\" value=\"%s\">"
"</div>"
"<br>"
"<div>"
"<label for=\"lati\">Latitude</label>"
"&nbsp;"
"<input type=\"number\" id=\"lati\" name=\"lati\" step=\"0.000001\" value=\"%f\">"
"</div>"
"<br>"
"<div>"
"<label for=\"long\">Longitude</label>"
"&nbsp;"
"<input type=\"number\" id=\"long\" name=\"long\" step=\"0.000001\" value=\"%f\">"
"</div>"
"<br>"
"<div>"
"<label for=\"apply\">New&nbsp;configuration</label>"
"&nbsp;"
"<input type=\"submit\" id=\"apply\" value=\"Apply\">"
"</div>"
"<br>"
"</form>"
"</div>"
"</body>"
"</html>"
;
static bool wifiConnected;
#else
static const char* cfgForm =
"<!DOCTYPE HTML>"
"<html>"
"<head>"
"<meta charset=\"UTF-8\">"
"<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">"
"<title>New&nbsp;configuration</title>"
"</head>"
"<body>"
"<div style=\"border-style:solid;border-width:1px;border-color:black;padding-left:10px\">"
"<form action=\"\" method=\"POST\">"
"<br>"
"<div>"
"<label for=\"ssid\">SSID</label>"
"&nbsp;"
"<input type=\"text\" id=\"ssid\" name=\"ssid\" value=\"%s\">"
"</div>"
"<br>"
"<div>"
"<label for=\"pass\">Password</label>"
"&nbsp;"
"<input type=\"password\" id=\"pass\" name=\"pass\" value=\"%s\">"
"</div>"
"<br>"
"<div>"
"<label for=\"tokn\">Token</label>"
"&nbsp;"
"<input type=\"text\" id=\"tokn\" name=\"tokn\" value=\"%s\">"
"</div>"
"<br>"
"<div>"
"<label for=\"lati\">Latitude</label>"
"&nbsp;"
"<input type=\"number\" id=\"lati\" name=\"lati\" step=\"0.000001\" value=\"%f\">"
"</div>"
"<br>"
"<div>"
"<label for=\"long\">Longitude</label>"
"&nbsp;"
"<input type=\"number\" id=\"long\" name=\"long\" step=\"0.000001\" value=\"%f\">"
"</div>"
"<br>"
"<div>"
"<label for=\"apply\">New&nbsp;configuration</label>"
"&nbsp;"
"<input type=\"submit\" id=\"apply\" value=\"Apply\">"
"</div>"
"<br>"
"</form>"
"</div>"
"</body>"
"</html>"
;
static char tbTokn[32];
static esp_mqtt_client_handle_t mqttClient = NULL;
static bool mqttConnected;
#endif

static char frmBuf[2048];
static char myName[32];
static char wifiSsid[32];
static char wifiPass[64];
float fLati;
float fLong;



static esp_err_t loadWifiInfo(uint8_t *pSsid, size_t *plSsid, uint8_t *pPass, size_t *plPass, 
#ifndef PUBLISH_VIA_AERK_PROXY
        uint8_t *pTokn, size_t *plTokn, 
#endif
        float *pfLati, float *pfLong) {
    nvs_handle nvsh;
    size_t lCoor;
    esp_err_t ee = nvs_open("maf", NVS_READONLY, &nvsh);
    if(ee == ESP_OK) {
        ee = nvs_get_blob(nvsh, "ssid", pSsid, plSsid);
        if(ee == ESP_OK) {
            ee = nvs_get_blob(nvsh, "pass", pPass, plPass);
            if(ee == ESP_OK) {
#ifndef PUBLISH_VIA_AERK_PROXY
                ee = nvs_get_blob(nvsh, "tokn", pTokn, plTokn);
                if(ee == ESP_OK) {
#endif
                    lCoor = 4;
                    ee = nvs_get_blob(nvsh, "lati", pfLati, &lCoor);
                    if(ee == ESP_OK) {
                        lCoor = 4;
                        ee = nvs_get_blob(nvsh, "long", pfLong, &lCoor);
                    }
#ifndef PUBLISH_VIA_AERK_PROXY
                }
#endif
            }
        }
        nvs_close(nvsh);
    }
    ESP_LOGI(TAG, "loadWifiInfo %X", ee);
    if(ee != ESP_OK) {
        *pSsid = 0;
        *plSsid = 0;
        *pPass = 0;
        *plPass = 0;
#ifndef PUBLISH_VIA_AERK_PROXY
        *pTokn = 0;
        *plTokn = 0;
#endif
        *pfLati = INFINITY;
        *pfLong = INFINITY;
    }
    return ee;
}

static esp_err_t saveWifiInfo(uint8_t *pSsid, size_t lSsid, uint8_t *pPass, size_t lPass, 
#ifndef PUBLISH_VIA_AERK_PROXY
        uint8_t *pTokn, size_t lTokn, 
#endif
        float *pfLati, float *pfLong) {
    nvs_handle nvsh;
    esp_err_t ee = nvs_open("maf", NVS_READWRITE, &nvsh);
    if(ee == ESP_OK) {
        ee = nvs_set_blob(nvsh, "ssid", pSsid, lSsid);
        if(ee == ESP_OK) {
            ee = nvs_set_blob(nvsh, "pass", pPass, lPass);
            if(ee == ESP_OK) {
#ifndef PUBLISH_VIA_AERK_PROXY
                ee = nvs_set_blob(nvsh, "tokn", pTokn, lTokn);
                if(ee == ESP_OK) {
#endif
                    ee = nvs_set_blob(nvsh, "lati", pfLati, 4);
                    if(ee == ESP_OK) {
                        ee = nvs_set_blob(nvsh, "long", pfLong, 4);
                        if(ee == ESP_OK) {
                            ee = nvs_commit(nvsh);
                        }
                    }
#ifndef PUBLISH_VIA_AERK_PROXY
                }
#endif
            }
        }
        nvs_close(nvsh);
    }
    ESP_LOGI(TAG, "saveWifiInfo %X", ee);
    return ee;
}

esp_err_t getSlashHandler(httpd_req_t *req) {
    snprintf(frmBuf, sizeof(frmBuf), cfgForm, wifiSsid, wifiPass, 
#ifndef PUBLISH_VIA_AERK_PROXY
            tbTokn, 
#endif
            fLati, fLong);
    httpd_resp_send(req, frmBuf, strlen(frmBuf));
    return ESP_OK;
}

httpd_uri_t getSlash = {
    .uri       = "/",
    .method    = HTTP_GET,
    .handler   = getSlashHandler,
    .user_ctx  = NULL
};

esp_err_t postSlashHandler(httpd_req_t *req) {
    int ret, remaining = req->content_len;
    char * buf = malloc(remaining + 3);
    char * pbuf = buf + 1;
    
    while (remaining > 0) {
        /* Read the data for the request */
        if ((ret = httpd_req_recv(req, pbuf,
                        remaining)) <= 0) {
            if (ret == HTTPD_SOCK_ERR_TIMEOUT) {
                /* Retry receiving if timeout occurred */
                continue;
            }
            return ESP_FAIL;
        }
        remaining -= ret;
    }   
    buf[0] = '&';
    int cs = 1;
    int cd = 1;
    while(cs <= req->content_len) {
        if(buf[cs] == '%') {
            cs++;
            buf[cd] = (buf[cs] > '9' ? buf[cs] - 55 : buf[cs] - 48) << 4;
            cs++;
            buf[cd] |= (buf[cs] > '9' ? buf[cs] - 55 : buf[cs] - 48) & 0xF;
        } else {
            buf[cd] = buf[cs];
        }
        cs++;
        cd++;
    }
    buf[cd++] = '&';
    buf[cd++] = 0;
    ESP_LOGI(TAG, "postSlashHandler '%s'", buf);    
    char *ssid;
    char *pass;
#ifndef PUBLISH_VIA_AERK_PROXY
    char *tken;
#endif
    char *lati;
    char *lon9;
    bool ok = false;
    ssid = strstr(buf, "&ssid=");
    if(ssid != NULL) {
        pass = strstr(buf, "&pass=");
        if(pass != NULL) {
#ifndef PUBLISH_VIA_AERK_PROXY
            tken = strstr(buf, "&tokn=");
            if(tken != NULL) {
#endif
                lati = strstr(buf, "&lati=");
                if(lati != NULL) {
                    lon9 = strstr(buf, "&long=");
                    if(lon9 != NULL) {
                        pbuf = strstr(ssid + 1, "&");
                        if(pbuf != NULL) {
                            *pbuf = 0;
                            pbuf = strstr(pass + 1, "&");
                            if(pbuf != NULL) {
                                *pbuf = 0;
#ifndef PUBLISH_VIA_AERK_PROXY
                                pbuf = strstr(tken + 1, "&");
                                if(pbuf != NULL) {
#endif
                                    *pbuf = 0;
                                    pbuf = strstr(lati + 1, "&");
                                    if(pbuf != NULL) {
                                        *pbuf = 0;
                                        pbuf = strstr(lon9 + 1, "&");
                                        if(pbuf != NULL) {
                                            *pbuf = 0;
                                            ssid += 6;
                                            pass += 6;
#ifndef PUBLISH_VIA_AERK_PROXY
                                            tken += 6;
#endif
                                            fLati = atof(lati + 6);
                                            fLong = atof(lon9 + 6);
                                            ok = true;
                                        }
                                    }
#ifndef PUBLISH_VIA_AERK_PROXY
                                }
#endif
                            }
                        }
                    }
                }
#ifndef PUBLISH_VIA_AERK_PROXY
            }
#endif
        }
    }
    httpd_resp_set_hdr(req, "Location", "/");
    httpd_resp_set_status(req, "303 See Other");
    httpd_resp_set_type(req, HTTPD_TYPE_TEXT);
    httpd_resp_send(req, NULL, 0);
    if(ok) {
        ESP_ERROR_CHECK(esp_wifi_disconnect());
        wifi_config_t wifiConfig = {
            .sta = {
                .ssid = "",
                .password = "",
            },
        };
        strlcpy((char *) wifiConfig.sta.ssid, ssid, sizeof(wifiConfig.sta.ssid));
        strlcpy((char *) wifiConfig.sta.password, pass, sizeof(wifiConfig.sta.password));
        saveWifiInfo((uint8_t *) ssid, strlen(ssid) + 1, (uint8_t *) pass, strlen(pass) + 1, 
#ifndef PUBLISH_VIA_AERK_PROXY
                (uint8_t *) tken, strlen(tken) + 1, 
#endif
                &fLati, &fLong);
        ESP_LOGI(TAG, "Setting WiFi configuration SSID '%s' (%s)...", wifiConfig.sta.ssid, wifiConfig.sta.password);
        ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifiConfig));
        ESP_ERROR_CHECK(esp_wifi_connect());

        strlcpy(wifiSsid, ssid, sizeof(wifiSsid));
        strlcpy(wifiPass, pass, sizeof(wifiPass));
#ifndef PUBLISH_VIA_AERK_PROXY
        strlcpy(tbTokn, tken, sizeof(tbTokn));
#endif
    }
    free(buf);
    return ESP_OK;
}

httpd_uri_t postSlash = {
    .uri       = "/",
    .method    = HTTP_POST,
    .handler   = postSlashHandler,
    .user_ctx  = NULL
};



httpd_handle_t startWebserver(void) {
    httpd_handle_t server = NULL;
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();

    // Start the httpd server
    ESP_LOGI(TAG, "Starting server on port: '%d'", config.server_port);
    if (httpd_start(&server, &config) == ESP_OK) {
        // Set URI handlers
        ESP_LOGI(TAG, "Registering URI handlers");
        httpd_register_uri_handler(server, &getSlash);
        httpd_register_uri_handler(server, &postSlash);
        return server;
    }

    ESP_LOGI(TAG, "Error starting server!");
    return NULL;
}

void stopWebserver(httpd_handle_t server) {
    // Stop the httpd server
    httpd_stop(server);
}

#ifndef PUBLISH_VIA_AERK_PROXY
static esp_err_t mqttEventHandler(esp_mqtt_event_handle_t event) {
    switch (event->event_id) {
        case MQTT_EVENT_CONNECTED:
            ESP_LOGI(TAG, "MQTT_EVENT_CONNECTED");
            mqttConnected = true;
            xEventGroupSetBits(egTelemetry, BIT1);
            break;
        case MQTT_EVENT_DISCONNECTED:
            ESP_LOGI(TAG, "MQTT_EVENT_DISCONNECTED");
            xEventGroupSetBits(egTelemetry, BIT0);
            mqttConnected = false;
            break;
        case MQTT_EVENT_SUBSCRIBED:
            ESP_LOGI(TAG, "MQTT_EVENT_SUBSCRIBED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_UNSUBSCRIBED:
            ESP_LOGI(TAG, "MQTT_EVENT_UNSUBSCRIBED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_PUBLISHED:
            ESP_LOGI(TAG, "MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_DATA:
            ESP_LOGI(TAG, "MQTT_EVENT_DATA");
            printf("TOPIC=%.*s\r\n", event->topic_len, event->topic);
            printf("DATA=%.*s\r\n", event->data_len, event->data);
            break;
        case MQTT_EVENT_ERROR:
            ESP_LOGI(TAG, "MQTT_EVENT_ERROR");
            break;
    }
    return ESP_OK;
}
#else
static esp_err_t http_client_event_handle(esp_http_client_event_t *evt) {
    switch(evt->event_id) {
        case HTTP_EVENT_ERROR:
            ESP_LOGI(TAG, "HTTP_EVENT_ERROR");
            break;
        case HTTP_EVENT_ON_CONNECTED:
            ESP_LOGI(TAG, "HTTP_EVENT_ON_CONNECTED");
            break;
        case HTTP_EVENT_HEADER_SENT:
            ESP_LOGI(TAG, "HTTP_EVENT_HEADER_SENT");
            break;
        case HTTP_EVENT_ON_HEADER:
            ESP_LOGI(TAG, "HTTP_EVENT_ON_HEADER");
            printf("%.*s", evt->data_len, (char*)evt->data);
            break;
        case HTTP_EVENT_ON_DATA:
            ESP_LOGI(TAG, "HTTP_EVENT_ON_DATA, len=%d", evt->data_len);
            if (!esp_http_client_is_chunked_response(evt->client)) {
                printf("%.*s", evt->data_len, (char*)evt->data);
            }

            break;
        case HTTP_EVENT_ON_FINISH:
            ESP_LOGI(TAG, "HTTP_EVENT_ON_FINISH");
            break;
        case HTTP_EVENT_DISCONNECTED:
            ESP_LOGI(TAG, "HTTP_EVENT_DISCONNECTED");
            break;
    }
    return ESP_OK;
}
#endif

static esp_err_t wifiEventHandler(void *ctx, system_event_t *event) {
    httpd_handle_t *server = (httpd_handle_t *) ctx;
    /* For accessing reason codes in case of disconnection */
    system_event_info_t *info = &event->event_info;

    switch(event->event_id) {
        case SYSTEM_EVENT_AP_START:
            ESP_LOGI(TAG, "SYSTEM_EVENT_AP_START");
            if (*server == NULL) {
                *server = startWebserver();
            }
            break;
        case SYSTEM_EVENT_AP_STOP:
            ESP_LOGI(TAG, "SYSTEM_EVENT_AP_STOP");
            if (*server) {
                stopWebserver(*server);
                *server = NULL;
            }
            break;
    case SYSTEM_EVENT_STA_START:
        ESP_LOGI(TAG, "SYSTEM_EVENT_STA_START");
        ESP_ERROR_CHECK(esp_wifi_connect());
        break;
    case SYSTEM_EVENT_STA_GOT_IP:
        ESP_LOGI(TAG, "SYSTEM_EVENT_STA_GOT_IP");
        ESP_LOGI(TAG, "Got IP: '%s'",
                ip4addr_ntoa(&event->event_info.got_ip.ip_info.ip));
#ifndef PUBLISH_VIA_AERK_PROXY
        if(mqttClient == NULL) {
            mqttConnected = false;
            const esp_mqtt_client_config_t mqttCfg = {
                .uri = "mqtt://11.0.0.244:11883",
                .event_handle = mqttEventHandler,
                .client_id = myName,
                .username = tbTokn,
                .password = NULL,
            };
            mqttClient = esp_mqtt_client_init(&mqttCfg);
            esp_mqtt_client_start(mqttClient);
        }
#else
        wifiConnected = true;
        xEventGroupSetBits(egTelemetry, BIT1);
#endif
        xEventGroupSetBits(egComms, BIT1);
        break;
    case SYSTEM_EVENT_STA_DISCONNECTED:
#ifndef PUBLISH_VIA_AERK_PROXY
        mqttConnected = false;
#else
        wifiConnected = false;
#endif
        xEventGroupSetBits(egComms, BIT0);
        ESP_LOGI(TAG, "SYSTEM_EVENT_STA_DISCONNECTED");
        ESP_LOGE(TAG, "Disconnect reason : %d", info->disconnected.reason);
        if (info->disconnected.reason == WIFI_REASON_BASIC_RATE_NOT_SUPPORT) {
            /*Switch to 802.11 bgn mode */
            esp_wifi_set_protocol(ESP_IF_WIFI_STA, WIFI_PROTOCAL_11B | WIFI_PROTOCAL_11G | WIFI_PROTOCAL_11N);
        }
        ESP_ERROR_CHECK(esp_wifi_connect());
#ifndef PUBLISH_VIA_AERK_PROXY
        if(mqttClient != NULL) {
            esp_mqtt_client_stop(mqttClient);
            mqttClient = NULL;
        }
#else
        xEventGroupSetBits(egTelemetry, BIT0);
#endif
        break;
    default:
        break;
    }
    return ESP_OK;
}

esp_err_t commsInit(void) {
    uint8_t mac[6];
    esp_efuse_mac_get_default(mac);
    snprintf(myName, sizeof(myName), "CF8266-%02X%02X%02X%02X%02X%02X", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
    ESP_LOGI(TAG, "start %s", myName);
    
    static httpd_handle_t server = NULL;
    tcpip_adapter_init();
    ESP_ERROR_CHECK(esp_event_loop_init(wifiEventHandler, &server));
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));
    ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM));
    
    nvs_handle nvsh;
    esp_err_t ee = nvs_open("maf", NVS_READONLY, &nvsh);
    ESP_LOGI(TAG, "nvs_open %X", ee);
    if(ee == ESP_OK) {
        char sspp[32];
        size_t spl = 32;
        ee = nvs_get_str(nvsh, "ssid", sspp, &spl);
        ESP_LOGI(TAG, "nvs_get_str %d", ee);
        nvs_close(nvsh);
        ESP_LOGI(TAG, "nvs_close");
    }
    
    wifi_config_t wifiConfig = {};
    size_t lSsid = sizeof(wifiSsid) - 1;
    size_t lPass = sizeof(wifiPass) - 1;
#ifndef PUBLISH_VIA_AERK_PROXY
    size_t lTokn = sizeof(tbTokn) - 1;
#endif
    wifiConnected = false;
    ee = loadWifiInfo((uint8_t *) wifiSsid, &lSsid, (uint8_t *) wifiPass, &lPass, 
#ifndef PUBLISH_VIA_AERK_PROXY
            (uint8_t *) tbTokn, &lTokn, 
#endif
            &fLati, &fLong);
    if(ee != ESP_OK) {
        ESP_LOGI(TAG, "No configuration on NVS");
        fLati = -34.584559;
        fLong = -58.494076;
        strlcpy(wifiSsid, "kepler10", sizeof(wifiSsid));
        strlcpy(wifiPass, "!0+2020==", sizeof(wifiPass));
#ifndef PUBLISH_VIA_AERK_PROXY
        strlcpy(tbTokn, "HAGysG8MhliIdRo1bhIH", sizeof(tbTokn));
#endif
    }
    strlcpy((char *) wifiConfig.sta.ssid, wifiSsid, sizeof(wifiConfig.sta.ssid));
    strlcpy((char *) wifiConfig.sta.password, wifiPass, sizeof(wifiConfig.sta.password));
    ESP_LOGI(TAG, "Setting WiFi configuration SSID %s...", wifiConfig.sta.ssid);
    
    wifi_config_t apConfig = {
        .ap = {
            .authmode = WIFI_AUTH_OPEN,
            .beacon_interval = 100,
            .channel = 6,
            .max_connection = 4,
            .password = "",
            .ssid = "",
            .ssid_hidden = 1,
            .ssid_len = 0
        }
    };
    strlcpy((char *) apConfig.ap.ssid, myName, sizeof(apConfig.ap.ssid));
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_APSTA));

    ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifiConfig));
    
    ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_AP, &apConfig));
    ESP_ERROR_CHECK(esp_wifi_start());
    
    return ESP_OK;
}

void commsTask(void* param) {
    bool ok = false;
    while(true) {
        EventBits_t bits = xEventGroupWaitBits(egComms, BIT0 | BIT1,
                            pdTRUE, pdFALSE, portMAX_DELAY);
        if(bits & BIT0) {
            if(ok) {
                sntp_stop();
                ok = false;
            }
        } else if(bits & BIT1) {
            if(!ok) {
                ok = true;
                ESP_LOGI(TAG, "Initializing SNTP");
                sntp_setoperatingmode(SNTP_OPMODE_POLL);
                sntp_setservername(0, "pool.ntp.org");
                sntp_init();
                // wait for time to be set
                time_t now = 0;
                struct tm timeinfo = { 0 };
                int retry = 0;
                const int retryCount = 10;
                while (timeinfo.tm_year < (2016 - 1900) && ++retry < retryCount) {
                    ESP_LOGI(TAG, "Waiting for system time to be set... (%d/%d)", retry, retryCount);
                    vTaskDelay(2000 / portTICK_PERIOD_MS);
                    time(&now);
                    localtime_r(&now, &timeinfo);
                }
            }
        }
    }
}

esp_err_t commsPublishData(char* data) {
    esp_err_t ee = ESP_OK;
#ifndef PUBLISH_VIA_AERK_PROXY
    if(mqttConnected) {
        int i = esp_mqtt_client_publish(mqttClient, AERK_MQTT_TOPIC, data, strlen(data), 0, 0);
        ESP_LOGI(TAG, "Publishing %d", i);
    }
#else
    if(wifiConnected) {
        esp_http_client_config_t config = {
            .url = AERK_PROXY_URL,
            .event_handler = http_client_event_handle,
            .method = HTTP_METHOD_POST,
            .cert_pem = NULL,
        };
        esp_http_client_handle_t client = esp_http_client_init(&config);
        esp_http_client_set_header(client, "X-AERK-ID", myName);
        esp_http_client_set_header(client, "Content-Type", "application/json");
        esp_http_client_set_post_field(client, data, strlen(data));
        ee = esp_http_client_perform(client);
        if (ee == ESP_OK) {
            ESP_LOGI(TAG, "Status = %d, content_length = %d",
            esp_http_client_get_status_code(client),
            esp_http_client_get_content_length(client));
        }
        esp_http_client_cleanup(client);
    }
#endif
    return ee;
}
