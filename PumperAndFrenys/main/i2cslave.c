/*
 * The MIT License (MIT)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files 
 * (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, 
 * publish, distribute, sublicense, and/or sell copies of the Software, 
 * and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be 
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY 
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/* 
 * File:   i2cslave.c
 *
 * Created on 10 de enero de 2020, 11:27
 */

#include <esp_log.h>
#include "i2cdev.h"

static const char *TAG = "I2C_SLAVE";

int i2c_slave_write(uint8_t bus, uint8_t slave_addr, const uint8_t *data, const uint8_t *buf, uint32_t len) {
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, slave_addr << 1, true);
    if (data)
        i2c_master_write(cmd, data, 1, true);
    i2c_master_write(cmd, buf, len, true);
    i2c_master_stop(cmd);
    esp_err_t res = i2c_master_cmd_begin(bus, cmd, CONFIG_I2CDEV_TIMEOUT / portTICK_RATE_MS);
    if (res != ESP_OK) {
        ESP_LOGE(TAG, "Could not write to device [0x%02x at %d]: %d", slave_addr, bus, res);
    }
    i2c_cmd_link_delete(cmd);
    return res == ESP_OK ? 0 : -5;
}

int i2c_slave_read(uint8_t bus, uint8_t slave_addr, const uint8_t *data, uint8_t *buf, uint32_t len) {
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    if (data != NULL) {
        i2c_master_start(cmd);
        i2c_master_write_byte(cmd, slave_addr << 1, true);
        i2c_master_write(cmd, data, 1, true);
    }
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (slave_addr << 1) | 1, true);
    i2c_master_read(cmd, buf, len, I2C_MASTER_LAST_NACK);
    i2c_master_stop(cmd);
    esp_err_t res = i2c_master_cmd_begin(bus, cmd, CONFIG_I2CDEV_TIMEOUT / portTICK_RATE_MS);
    if (res != ESP_OK) {
        ESP_LOGE(TAG, "Could not read from device [0x%02x at %d]: %d", slave_addr, bus, res);
    }
    i2c_cmd_link_delete(cmd);
    return res == ESP_OK ? 0 : -5;
}
