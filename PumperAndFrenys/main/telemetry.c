/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2020 by Daniel Quintela
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files 
 * (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, 
 * publish, distribute, sublicense, and/or sell copies of the Software, 
 * and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be 
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY 
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/* 
 * File:   telemetry.c
 * Author: danielq
 *
 * Created on 11 de enero de 2020, 10:32
 */

#include "paf.h"

#include "esp_system.h"
#include <sys/time.h>

#ifdef PAF_HAS_BME680
//#define BME680_FLOAT_POINT_COMPENSATION
#include "bme680.h"
#include "i2cdev.h"
#else
#include "bmp280.h"
#include "ccs811.h"
#endif

extern int idxSsid;

static const char *TAG = "telemetry";
static int k;
static volatile int state;
static struct timeval tv;
static struct timezone *tz;
static bool haveCnxn;
static SemaphoreHandle_t semWeather;
static SemaphoreHandle_t semI2c;
static SemaphoreHandle_t semState;
static struct HbbTelemetryEntry hteWeather;
#ifdef PAF_HAS_BME680
static struct bme680_dev bme;
static i2c_dev_t i2cdBme;
#else
static bmp280_t bme;
static ccs811_sensor_t *ccs;
static uint16_t difTvoc;
static int eqlsTvoc;
#endif

#ifdef PMS_UART_NUM

static const uint8_t
    SpmAct[] = {0x42,0x4D,0xE1,0x00,0x01,0x01,0x71}, // set active mode
    SpmSlp[] = {0x42,0x4D,0xE4,0x00,0x00,0x01,0x73}, // sleep
    SpmWak[] = {0x42,0x4D,0xE4,0x00,0x01,0x01,0x74}, // wake
    SpmCfg[] = {0x42,0x4D,0xE1,0x00,0x00,0x01,0x70}, // set passive mode
    SpmTrg[] = {0x42,0x4D,0xE2,0x00,0x00,0x01,0x71}; // passive mode read

static void SpmInit() {
    uart_write_bytes(PMS_UART_NUM, (const char *) SpmCfg, 7);
}

static void SpmWakeUp() {
    uart_write_bytes(PMS_UART_NUM, (const char *) SpmWak, 7);
}

static void SpmGoSleep() {
    uart_write_bytes(PMS_UART_NUM, (const char *) SpmSlp, 7);
}

static void SpmPassiveModeRead() {
    uart_flush_input(PMS_UART_NUM);
    uart_write_bytes(PMS_UART_NUM, (const char *) SpmTrg, 7);
    vTaskDelay(100 / portTICK_PERIOD_MS);
    uint8_t data[PMS_FRAME_SIZE];
    int o = 0;
    while(o < sizeof(data)) {
        int l = uart_read_bytes(PMS_UART_NUM, data + o, sizeof(data) - o, 1500 / portTICK_RATE_MS);
        if(l < 1) {
            ESP_LOGE(TAG, "SpmPassiveModeRead return only %d bytes", o);
            return;
        }
        o += l;
    }
    ESP_LOGI(TAG, "SpmPassiveModeRead read data OK");
    ESP_LOG_BUFFER_HEXDUMP(TAG, data, PMS_FRAME_SIZE, ESP_LOG_INFO);
    // calculamos el check code
    uint16_t ccc = ((uint16_t) data[30]) << 8;
    ccc |= data[31];
    for(int i = 0, l = PMS_FRAME_SIZE - 2; i < l; i++) {
        ccc -= data[i];
    }
    ESP_LOGI(TAG, "ccc 0x%X", ccc);
    if(ccc == 0) {
        // el check code calculado es igual al recibido: extraemos los datos
        int t10 = ((int) data[4]) << 8;
        t10 |= data[5];
        int t25 = ((int) data[6]) << 8;
        t25 |= data[7];
        int t100 = ((int) data[8]) << 8;
        t100 |= data[9];
        ESP_LOGI(TAG, "TSI PM1.0 %d, PM2.5 %d, PM10 %d", 
                t10, t25, t100);
        gettimeofday(&tv, &tz);
        ESP_LOGI(TAG, "time %ld.%06ld", tv.tv_sec, tv.tv_usec);
        if(tv.tv_sec > 1500000000) {
            xSemaphoreTake(semWeather, portMAX_DELAY);
            struct HbbTelemetryEntry hte = {
                .flags = HTE_HAS_PM | 
#ifdef PAF_HAS_BME680
                        HTE_HAS_TVOC
#else
                        (ccs == NULL ? 0 : HTE_HAS_TVOC)
#endif
                        ,
                .ts = tv.tv_sec,
                .t = hteWeather.t,
                .h = hteWeather.h,
                .p = hteWeather.p,
                .co2 = hteWeather.co2,
                .tvoc = hteWeather.tvoc,
                .pm10 = t10,
                .pm25 = t25,
                .pm100 = t100
            };
            xSemaphoreGive(semWeather);
            ESP_LOGI(TAG, "CO2 %d, VOC %d, humedad %.1f, temperatura %.1f", 
                    hte.co2, hte.tvoc, hte.h, hte.t);
            hte.ts *= 1000;
            hte.ts += tv.tv_usec / 1000;
            if(setEntryPosition(&hte) == ESP_OK) {
                commsPublishData(&hte);
            }
        }
    } else {
        int t10 = 0;
        int t25 = 0;
        int t100 = 0;
        ESP_LOGI(TAG, "TSI PM1.0 %d, PM2.5 %d, PM10 %d", 
                t10, t25, t100);
        gettimeofday(&tv, &tz);
        ESP_LOGI(TAG, "time %ld.%06ld", tv.tv_sec, tv.tv_usec);
        if(tv.tv_sec > 1500000000) {
            xSemaphoreTake(semWeather, portMAX_DELAY);
            struct HbbTelemetryEntry hte = {
                .flags = 0 | 
#ifdef PAF_HAS_BME680
                        HTE_HAS_TVOC
#else
                        (ccs == NULL ? 0 : HTE_HAS_TVOC)
#endif
                        ,
                .ts = tv.tv_sec,
                .t = hteWeather.t,
                .h = hteWeather.h,
                .p = hteWeather.p,
                .co2 = hteWeather.co2,
                .tvoc = hteWeather.tvoc,
                .pm10 = t10,
                .pm25 = t25,
                .pm100 = t100
            };
            xSemaphoreGive(semWeather);
            ESP_LOGI(TAG, "CO2 %d, VOC %d, humedad %.1f, temperatura %.1f", 
                    hte.co2, hte.tvoc, hte.h, hte.t);
            hte.ts *= 1000;
            hte.ts += tv.tv_usec / 1000;
            if(setEntryPosition(&hte) == ESP_OK) {
                commsPublishData(&hte);
            }
        }
    }
    ESP_LOGI(TAG, "esp_get_free_heap_size 0x%X", esp_get_free_heap_size());
}

#endif

#ifdef PAF_HAS_BME680
static void user_delay_ms(uint32_t period) {
    /*
     * Return control or wait,
     * for a period amount of milliseconds
     */
    vTaskDelay(period / portTICK_PERIOD_MS);
}

static int8_t user_i2c_read(uint8_t dev_id, uint8_t reg_addr, uint8_t *reg_data, uint16_t len) {
//    int8_t rslt = 0; /* Return 0 for Success, non-zero for failure */

    /*
     * The parameter dev_id can be used as a variable to store the I2C address of the device
     */

    /*
     * Data on the bus should be like
     * |------------+---------------------|
     * | I2C action | Data                |
     * |------------+---------------------|
     * | Start      | -                   |
     * | Write      | (reg_addr)          |
     * | Stop       | -                   |
     * | Start      | -                   |
     * | Read       | (reg_data[0])       |
     * | Read       | (....)              |
     * | Read       | (reg_data[len - 1]) |
     * | Stop       | -                   |
     * |------------+---------------------|
     */

    xSemaphoreTake(semI2c, portMAX_DELAY);
    esp_err_t ee = i2c_dev_read_reg((const i2c_dev_t *) &i2cdBme, reg_addr, reg_data, len);
    xSemaphoreGive(semI2c);
    
    return ee == ESP_OK ? BME680_OK : INT8_C(-9);
}

static int8_t user_i2c_write(uint8_t dev_id, uint8_t reg_addr, uint8_t *reg_data, uint16_t len) {
//    int8_t rslt = 0; /* Return 0 for Success, non-zero for failure */

    /*
     * The parameter dev_id can be used as a variable to store the I2C address of the device
     */

    /*
     * Data on the bus should be like
     * |------------+---------------------|
     * | I2C action | Data                |
     * |------------+---------------------|
     * | Start      | -                   |
     * | Write      | (reg_addr)          |
     * | Write      | (reg_data[0])       |
     * | Write      | (....)              |
     * | Write      | (reg_data[len - 1]) |
     * | Stop       | -                   |
     * |------------+---------------------|
     */

    xSemaphoreTake(semI2c, portMAX_DELAY);
    esp_err_t ee = i2c_dev_write_reg((const i2c_dev_t *) &i2cdBme, reg_addr, reg_data, len);
    xSemaphoreGive(semI2c);
    
    return ee == ESP_OK ? BME680_OK : INT8_C(-9);
}

static esp_err_t bme680Read(void) {
    struct bme680_field_data data;
//    ESP_LOGI(TAG, "bme680Read before xSemaphoreTake");
//    xSemaphoreTake(semI2c, portMAX_DELAY);
//    ESP_LOGI(TAG, "bme680Read after xSemaphoreTake");
    int8_t rslt = bme680_get_sensor_data(&data, &bme);
//    ESP_LOGI(TAG, "bme680Read after bme680_get_sensor_data");
//    xSemaphoreGive(semI2c);
    ESP_LOGI(TAG, "bme680_get_sensor_data 0x%X %f %f %f\n", rslt, data.temperature, data.pressure, data.humidity);
    if(rslt == BME680_OK) {
        hteWeather.h = data.humidity;
        hteWeather.p = data.pressure / 100.0;
        hteWeather.t = data.temperature;
        if(data.status & BME680_GASM_VALID_MSK) {
            hteWeather.tvoc = data.gas_resistance; // f(data.gas_resistance);
            ESP_LOGI(TAG, "bme680_get_sensor_data gas resistance %f", data.gas_resistance);
        }
    }
    ESP_LOGI(TAG, "bme680_set_sensor_mode 0x%X", bme680_set_sensor_mode(&bme));
    return rslt == BME680_OK ? ESP_OK : ESP_FAIL;
}

static esp_err_t bme680Init(void) {
    bme.dev_id = BME680_I2C_ADDR_PRIMARY;
    bme.intf = BME680_I2C_INTF;
    bme.read = user_i2c_read;
    bme.write = user_i2c_write;
    bme.delay_ms = user_delay_ms;
    /* amb_temp can be set to 25 prior to configuring the gas sensor 
     * or by performing a few temperature readings without operating the gas sensor.
     */
    bme.amb_temp = 25;
    i2cdBme.port = I2C_MASTER_NUM;
    i2cdBme.addr = bme.dev_id;
    int8_t rslt = BME680_OK;
    rslt = bme680_init(&bme);
    if(rslt == BME680_OK) {
        uint8_t set_required_settings;

        /* Set the temperature, pressure and humidity settings */
        bme.tph_sett.os_hum = BME680_OS_1X; // BME680_OS_2X;
        bme.tph_sett.os_pres = BME680_OS_16X; // BME680_OS_4X;
        bme.tph_sett.os_temp = BME680_OS_2X; // BME680_OS_8X;
        bme.tph_sett.filter = BME680_FILTER_SIZE_0; // BME680_FILTER_SIZE_3;

        /* Set the remaining gas sensor settings and link the heating profile */
        bme.gas_sett.run_gas = BME680_ENABLE_GAS_MEAS;
        /* Create a ramp heat waveform in 3 steps */
        bme.gas_sett.heatr_temp = 300; // 320; /* degree Celsius */
        bme.gas_sett.heatr_dur = 100; // 150; /* milliseconds */

        /* Select the power mode */
        /* Must be set before writing the sensor configuration */
        bme.power_mode = BME680_FORCED_MODE; 

        /* Set the required sensor settings needed */
        set_required_settings = BME680_OST_SEL | BME680_OSP_SEL | BME680_OSH_SEL | BME680_FILTER_SEL 
            | BME680_GAS_SENSOR_SEL;

        /* Set the desired sensor configuration */
        rslt = bme680_set_sensor_settings(set_required_settings,&bme);

        if(rslt == BME680_OK) {
            /* Set the power mode */
            rslt = bme680_set_sensor_mode(&bme);
        }
    }
    return rslt == BME680_OK ? ESP_OK : ESP_FAIL;
}
#else
static void ccs811SetWeather(void) {
    xSemaphoreTake(semI2c, portMAX_DELAY);
    bool ok = ccs811_set_environmental_data(ccs, hteWeather.t, hteWeather.h);
    ESP_LOGI(TAG, "ccs811_set_environmental_data %d", ok);
    xSemaphoreGive(semI2c);
}

static void ccs811Read(void) {
    if(ccs != NULL) {
        xSemaphoreTake(semI2c, portMAX_DELAY);
        bool ok = ccs811_get_results(ccs, &hteWeather.tvoc, &hteWeather.co2, NULL, NULL);
        ESP_LOGI(TAG, "ccs811_get_results %d 0x%X", ok, ccs->error_code);
        xSemaphoreGive(semI2c);
        if(ok) {
            if(hteWeather.tvoc == difTvoc) {
                eqlsTvoc += 1;
            } else {
                difTvoc = hteWeather.tvoc;
            }
        }
        if(! ok || eqlsTvoc > 60) {
            ESP_LOGE(TAG, "ccs811 reinit");
            free(ccs);
            ccs = NULL;
            commsPublishMessage(HTE_MSG_CCS_REINIT);
        }
    }
}

static esp_err_t bme280Read(void) {
    float temperature, pressure, humidity = 0.0;
    xSemaphoreTake(semI2c, portMAX_DELAY);
    esp_err_t ee = bmp280_read_float(&bme, &temperature, &pressure, &humidity);
    xSemaphoreGive(semI2c);
    ESP_LOGI(TAG, "bmp280_read_float %d %f %f %f\n", ee, temperature, pressure, humidity);
    if(ee == ESP_OK) {
        hteWeather.h = humidity;
        hteWeather.p = pressure / 100.0;
        hteWeather.t = temperature;
    }
    return ee;
}

static esp_err_t bme280Init(void) {
    bmp280_params_t params;
    bmp280_init_default_params(&params);
    memset(&bme, 0, sizeof(bmp280_t));
    esp_err_t ee = bmp280_init_desc(&bme, BMP280_I2C_ADDRESS_0, I2C_MASTER_NUM, I2C_MASTER_SDA_IO, I2C_MASTER_SCL_IO);
    ESP_LOGI(TAG, "bmp280_init_desc %d\n", ee);
    if(ee == ESP_OK) {
        xSemaphoreTake(semI2c, portMAX_DELAY);
        ee = bmp280_init(&bme, &params);
        xSemaphoreGive(semI2c);
        ESP_LOGI(TAG, "bmp280_init %d\n", ee);
    }    
    return ee;
}
#endif


static void pmsClose(void) {
    esp_err_t ret = 0;
#ifdef PMS_UART_NUM
    SpmGoSleep();
#else
    #ifdef I2C_PMS_ADDRESS
    gpio_set_level(PMS_SET_GPIO, 0);
    #endif
#endif
    ESP_LOGI(TAG, "Close measurement %d", ret);
}

static void pmsOpen(void) {
    esp_err_t ret = 0;
#ifdef PMS_UART_NUM
    SpmWakeUp();
#else
    #ifdef I2C_PMS_ADDRESS
    gpio_set_level(PMS_SET_GPIO, 1);
    #endif
#endif
    ESP_LOGI(TAG, "Open measurement %d", ret);
}

static void pmsReadAndPublish(void) {
#ifdef PMS_UART_NUM
    SpmPassiveModeRead();
#else
    #ifdef I2C_PMS_ADDRESS
    uint8_t data[PMS_FRAME_SIZE];
    xSemaphoreTake(semI2c, portMAX_DELAY);
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (I2C_PMS_ADDRESS << 1) | I2C_MASTER_READ, 1);
    i2c_master_read(cmd, data, PMS_FRAME_SIZE, 0);
    i2c_master_stop(cmd);
    esp_err_t ret = i2c_master_cmd_begin(I2C_MASTER_NUM, cmd, 1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);
    xSemaphoreGive(semI2c);
    ESP_LOGI(TAG, "Read data %d", ret);
    if(ret == ESP_OK) {
        ESP_LOG_BUFFER_HEXDUMP(TAG, data, PMS_FRAME_SIZE, ESP_LOG_INFO);
    }
    // calculamos el check code
    uint16_t ccc = ((uint16_t) data[30]) << 8;
    ccc |= data[31];
    for(int i = 0, l = PMS_FRAME_SIZE - 2; i < l; i++) {
        ccc -= data[i];
    }
    ESP_LOGI(TAG, "ccc 0x%X", ccc);
    if(ccc == 0) {
        // el check code calculado es igual al recibido: extraemos los datos
        int t10 = ((int) data[4]) << 8;
        t10 |= data[5];
        int t25 = ((int) data[6]) << 8;
        t25 |= data[7];
        int t100 = ((int) data[8]) << 8;
        t100 |= data[9];
        ESP_LOGI(TAG, "TSI PM1.0 %d, PM2.5 %d, PM10 %d", 
                t10, t25, t100);
        gettimeofday(&tv, &tz);
        ESP_LOGI(TAG, "time %ld.%06ld", tv.tv_sec, tv.tv_usec);
        if(tv.tv_sec > 1500000000) {
            xSemaphoreTake(semWeather, portMAX_DELAY);
            struct HbbTelemetryEntry hte = {
                .ts = tv.tv_sec,
                .t = hteWeather.t,
                .h = hteWeather.h,
                .p = hteWeather.p,
                .co2 = hteWeather.co2,
                .tvoc = hteWeather.tvoc,
                .pm10 = t10,
                .pm25 = t25,
                .pm100 = t100
            };
            xSemaphoreGive(semWeather);
            ESP_LOGI(TAG, "CO2 %d, VOC %d, humedad %.1f, temperatura %.1f", 
                    hte.co2, hte.tvoc, hte.h, hte.t);
            hte.ts *= 1000;
            hte.ts += tv.tv_usec / 1000;
            if(setEntryPosition(&hte) == ESP_OK) {
                commsPublishData(&hte);
            }
        }
    }
    ESP_LOGI(TAG, "esp_get_free_heap_size 0x%X", esp_get_free_heap_size());
    #endif
#endif
}

#ifdef PAF_HAS_BME680
static void weatherTask(void* param) {
    uint16_t meas_period;
    bme680_get_profile_dur(&meas_period, &bme);
    ESP_LOGI(TAG, "bme680_get_profile_dur %d", meas_period);
    while(true) {
        ESP_LOGI(TAG, "weatherTask to sleep");
        user_delay_ms(meas_period * 1100); // plus 10%
        ESP_LOGI(TAG, "weatherTask after sleep");
        
        bme680Read();
        ESP_LOGI(TAG, "weatherTask after read");
    }
}
#else
static void weatherTask(void* param) {
    TickType_t toBme = 10000 / portTICK_PERIOD_MS;
    TickType_t toCcs = 5000 / portTICK_PERIOD_MS;
    TickType_t toNxt = toBme;
    while(true) {
        vTaskDelay(toNxt);
        if(ccs == NULL) {
            difTvoc = 0xFFFF;
            eqlsTvoc = 0;
            ccs = ccs811_init_sensor(I2C_MASTER_NUM, CCS811_I2C_ADDRESS_1);
            ESP_LOGI(TAG, "ccs811_init_sensor 0x%X", (uint32_t) ccs);
        }
        if(state > 1) {
            xSemaphoreTake(semWeather, portMAX_DELAY);
            if(toNxt == toBme) {
                if(bme280Read() == ESP_OK) {
                    ccs811SetWeather();
                }
                toNxt = toCcs;
            } else {
                ccs811Read();
                toNxt = toBme;
            }
            xSemaphoreGive(semWeather);
        }
    }
}
#endif

esp_err_t telemetryInit(void) {
    semWeather = xSemaphoreCreateMutex();
    semI2c = xSemaphoreCreateMutex();
    semState = xSemaphoreCreateMutex();

#ifdef PMS_UART_NUM
    const uart_config_t uart_config = {
        .baud_rate = 9600,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
    };
//    // We won't use a buffer for sending data.
    esp_err_t eee = uart_param_config(PMS_UART_NUM, &uart_config);
    if(eee != ESP_OK) {
        ESP_LOGE(TAG, "uart_param_config 0x%X", eee);
    } else {
        eee = uart_set_pin(PMS_UART_NUM, PMS_TXD_PIN, PMS_RXD_PIN, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);
        if(eee != ESP_OK) {
            ESP_LOGE(TAG, "uart_set_pin 0x%X", eee);
        } else {
            eee = uart_driver_install(PMS_UART_NUM, 256, 0, 0, NULL, 0);
            if(eee != ESP_OK) {
                ESP_LOGE(TAG, "uart_driver_install 0x%X", eee);
            } else {
                ESP_LOGI(TAG, "PMS_UART OK!!!");
                SpmInit();
            }
        }
    }

//    SpmInit();
#endif


#ifdef PMS_SET_GPIO
    gpio_pad_select_gpio(PMS_SET_GPIO);
    gpio_set_direction(PMS_SET_GPIO, GPIO_MODE_OUTPUT);
#endif
    int i2c_master_port = I2C_MASTER_NUM;
    i2c_config_t conf;
    conf.mode = I2C_MODE_MASTER;
    conf.sda_io_num = I2C_MASTER_SDA_IO;
    conf.sda_pullup_en = GPIO_PULLUP_ENABLE;
    conf.scl_io_num = I2C_MASTER_SCL_IO;
    conf.scl_pullup_en = GPIO_PULLUP_ENABLE;
    conf.master.clk_speed = I2C_MASTER_FREQ_HZ;
    i2c_param_config(i2c_master_port, &conf);
    i2c_driver_install(i2c_master_port, conf.mode, I2C_MASTER_RX_BUF_DISABLE, I2C_MASTER_TX_BUF_DISABLE, 0);
    uiInit(I2C_MASTER_NUM, semI2c);
    haveCnxn = false;
    state = 0;
    k = 0;
#ifdef PAF_HAS_BME680
    esp_err_t ee = bme680Init();
    ESP_LOGI(TAG, "bme680Init 0x%X", ee);
#else
    esp_err_t ee = bme280Init();
    ESP_LOGI(TAG, "bme280Init 0x%X", ee);
#endif
    if(ee == ESP_OK) {
#ifndef PAF_HAS_BME680
        ccs = NULL;
#endif
        xTaskCreate(&weatherTask, "weather_task", 2048, NULL, 5, NULL);
    }
    uiShow(PAF_STATE_BOOTING, ee);
    return ESP_OK;
}

void telemetryTask(void* param) {
    
    while(true) {
        uint32_t qi;
        if(xQueueReceive(qTelemetry, &qi, 1000 / portTICK_PERIOD_MS)) {
            ESP_LOGI(TAG, "telemetryTask qi 0x%X", qi);
            switch(qi >> 24) {
                case PAF_TELEMETRY_CMD:
                    xSemaphoreTake(semState, portMAX_DELAY);
                    switch((qi & 0xFF0000) >> 16) {
                        case PAF_TELEMETRY_CMD_DISC:
                            uiShow(PAF_STATE_NOT_CONNECTED, 0);
                            haveCnxn = false;
                            state = 0;
                            break;
                        case PAF_TELEMETRY_CMD_CONN:
                            uiShow(PAF_STATE_CONNECTED, idxSsid + 1);
                            haveCnxn = true;
                            if(state == 1) {
                                state++;
                            }
                            break;
                    }
                    xSemaphoreGive(semState);
                    break;
                case PAF_TELEMETRY_MSG:
                    uiShow((qi & 0xFF0000) >> 16, qi & 0xFFFF);
                    break;
            }
        } else {
            k++;
            xSemaphoreTake(semState, portMAX_DELAY);
            switch(state) {
                case 0:
                    pmsClose();
                    k = 0;
                    if(haveCnxn) {
                        state += 2;
                    } else {
                        state++;
                    }
                    break;
                case 2:
                    if(k >= PMS_SECONDS_FOR_OPEN) {
                        pmsOpen();
                        state++;
                        uiShow(PAF_STATE_READING, 0);
                    }
                    break;
                case 3:
                    if(k >= PMS_SECONDS_FOR_READ) {
                        pmsReadAndPublish();
                        state = 0;
                        uiShow(PAF_STATE_IDLE, 0);
                    }
                    break;
            }            
            xSemaphoreGive(semState);
        }        
    }
}

void telemetryTaskCmd(uint8_t cmd) {
    ESP_LOGI(TAG, "telemetryTaskCmd 0x%X", cmd);
    uint32_t qi = (PAF_TELEMETRY_CMD << 24) | ((cmd & 0xFF) << 16);
    xQueueSendToBack(qTelemetry, &qi, portMAX_DELAY);
}

void telemetryTaskMsg(uint8_t type, uint32_t data) {
    ESP_LOGI(TAG, "telemetryTaskMsg 0x%X 0x%X", type, data);
    uint32_t qi = (PAF_TELEMETRY_MSG << 24) | ((type & 0xFF) << 16) | (data & 0xFFFF);
    xQueueSendToBack(qTelemetry, &qi, portMAX_DELAY);
}
