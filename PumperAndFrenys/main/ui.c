/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2020 by Daniel Quintela
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files 
 * (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, 
 * publish, distribute, sublicense, and/or sell copies of the Software, 
 * and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be 
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY 
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/* 
 * File:   ui.c
 * Author: danielq
 *
 * Created on 05 de febrero de 2020, 18:15
 */

#include "paf.h"

#ifdef UI_I2C_ADDRESS
// I have a OLED display: include support for it and declare working variables
#   include "ssd1366.h"
#   include "font8x8_basic.h"
static i2c_port_t uiI2cPort;
static bool uiOk;
static bool uiCleared;
static char text[32];
static const char * pafStateTexts[] = {
    "BOOTING",
    "CONTING",
    "NO CONN",
    "CONNTED",
    "IDLE   ",
    "READING"
};
static SemaphoreHandle_t semI2c;
static uint8_t zero[128];
#endif
#ifdef UI_RED_PIN
// I have one tri-color LED: declare working variables
static int blinks[6][2] = {
    {UI_RED_PIN, UI_RED_PIN},
    {UI_RED_PIN, UI_GREEN_PIN},
    {UI_RED_PIN, UI_RED_PIN},
    {UI_GREEN_PIN, UI_GREEN_PIN},
    {UI_GREEN_PIN, UI_BLUE_PIN},
    {UI_BLUE_PIN, UI_BLUE_PIN}    
};
static volatile int curState;
static volatile esp_err_t curError;
#endif
#ifdef PAF_AP_INACT_TIME
// I have one bi-color LED and two white LEDs: declare working variables
#include <time.h>
static time_t firstOn;
static time_t lastOn;
#endif

static const char *TAG = "ui";

#ifdef UI_RED_PIN
static void blinkTask(void* param) {
    // I turn off the tri-color LED
    gpio_set_level(UI_RED_PIN, 1);
    gpio_set_level(UI_GREEN_PIN, 1);
    gpio_set_level(UI_BLUE_PIN, 1);    
    curState = 0;
    while(true) {
        ESP_LOGI(TAG, "blinkTask %d 0x%X %d %d", curState, curError, blinks[curState][0], blinks[curState][1]);
        int blink = blinks[curState][0];
        gpio_set_level(blink, 0);
        vTaskDelay(250 / portTICK_PERIOD_MS);
        gpio_set_level(blink, 1);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
        blink = blinks[curState][1];
        gpio_set_level(blink, 0);
        vTaskDelay(250 / portTICK_PERIOD_MS);
        gpio_set_level(blink, 1);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
}
#endif
#ifdef PAF_AP_INACT_TIME
static void blinkDelay(int ms) {
    for(int o = 0; o < ms; o += 50) {
        vTaskDelay(50 / portTICK_PERIOD_MS);
        if(commsHasAp) {
            firstOn = 0;
        } else if(! gpio_get_level(UI_AP_START_BUTTON)) {
            if(! firstOn) {
                firstOn = time(NULL);
            }
            lastOn = time(NULL);
            if((lastOn - firstOn) >= 5) {
                ESP_LOGI(TAG, "blinkDelay detects AP button pressed");
                xEventGroupSetBits(egComms, BIT2);
                firstOn = lastOn;
            }
        } else if((time(NULL) - lastOn) >= 2) {
            firstOn = 0;
        }
    }
}
#else
static void blinkDelay(int ms) {
    vTaskDelay(ms / portTICK_PERIOD_MS);
}
#endif
#ifdef UI_WHITE_0_3MM
static void blinkTask(void* param) {
    gpio_set_level(UI_WHITE_0_3MM, 0);
    gpio_set_level(UI_RED_5MM, 0);
    gpio_set_level(UI_GREEN_5MM, 0);    
    gpio_set_level(UI_WHITE_1_3MM, 0);
    while(true) {
        ESP_LOGI(TAG, "blinkTask %d %d %d", commsHasIp, commsHasAp, positionFixed);
        //
        bool ok = ! commsHasAp;
        gpio_set_level(ok ? UI_GREEN_5MM : UI_RED_5MM, 1);
        blinkDelay(200);
        gpio_set_level(ok ? UI_GREEN_5MM : UI_RED_5MM, 0);
        blinkDelay(1000);
        //
        ok = commsHasIp;
        gpio_set_level(UI_WHITE_0_3MM, 1);
        gpio_set_level(ok ? UI_GREEN_5MM : UI_RED_5MM, 1);
        blinkDelay(200);
        gpio_set_level(ok ? UI_GREEN_5MM : UI_RED_5MM, 0);
        gpio_set_level(UI_WHITE_0_3MM, 0);
        blinkDelay(1000);
        //
        ok = positionFixed;
        gpio_set_level(UI_WHITE_1_3MM, 1);
        gpio_set_level(ok ? UI_GREEN_5MM : UI_RED_5MM, 1);
        blinkDelay(200);
        gpio_set_level(ok ? UI_GREEN_5MM : UI_RED_5MM, 0);
        gpio_set_level(UI_WHITE_1_3MM, 0);
        blinkDelay(1000);
    }
}
#endif

esp_err_t uiInit(i2c_port_t port, SemaphoreHandle_t sem) {
    esp_err_t espRc;
#ifdef UI_I2C_ADDRESS
    uiI2cPort = port;
    semI2c = sem;

    xSemaphoreTake(semI2c, portMAX_DELAY);
    
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();

    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (UI_I2C_ADDRESS << 1) | I2C_MASTER_WRITE, true);
    i2c_master_write_byte(cmd, OLED_CONTROL_BYTE_CMD_STREAM, true);

    i2c_master_write_byte(cmd, OLED_CMD_SET_CHARGE_PUMP, true);
    i2c_master_write_byte(cmd, 0x14, true);

    i2c_master_write_byte(cmd, OLED_CMD_SET_SEGMENT_REMAP, true); // reverse left-right mapping
    i2c_master_write_byte(cmd, OLED_CMD_SET_COM_SCAN_MODE, true); // reverse up-bottom mapping

    i2c_master_write_byte(cmd, OLED_CMD_DISPLAY_ON, true);
    i2c_master_stop(cmd);

    espRc = i2c_master_cmd_begin(uiI2cPort, cmd, 10 / portTICK_PERIOD_MS);
    if (espRc == ESP_OK) {
        uiOk = true;
        ESP_LOGI(TAG, "OLED configured successfully");
    } else {
        uiOk = false;
        uiI2cPort = -1;
        ESP_LOGE(TAG, "OLED configuration failed. code: 0x%.2X", espRc);
    }
    i2c_cmd_link_delete(cmd);
    uiCleared = false;
    
    xSemaphoreGive(semI2c);
#endif
#ifdef UI_RED_PIN
    gpio_pad_select_gpio(UI_RED_PIN);
    gpio_pad_select_gpio(UI_GREEN_PIN);
    gpio_pad_select_gpio(UI_BLUE_PIN);
    gpio_set_direction(UI_RED_PIN, GPIO_MODE_OUTPUT);
    gpio_set_direction(UI_GREEN_PIN, GPIO_MODE_OUTPUT);
    gpio_set_direction(UI_BLUE_PIN, GPIO_MODE_OUTPUT);
    xTaskCreate(&blinkTask, "blink_task", 2048, NULL, 7, NULL);
    espRc = ESP_OK;
#endif
#ifdef PAF_AP_INACT_TIME
    gpio_pad_select_gpio(UI_AP_START_BUTTON);
    gpio_set_direction(UI_AP_START_BUTTON, GPIO_MODE_INPUT);
#endif
#ifdef UI_WHITE_0_3MM
    gpio_pad_select_gpio(UI_WHITE_0_3MM);
    gpio_pad_select_gpio(UI_RED_5MM);
    gpio_pad_select_gpio(UI_GREEN_5MM);
    gpio_pad_select_gpio(UI_WHITE_1_3MM);
    gpio_set_direction(UI_WHITE_0_3MM, GPIO_MODE_OUTPUT);
    gpio_set_direction(UI_RED_5MM, GPIO_MODE_OUTPUT);
    gpio_set_direction(UI_GREEN_5MM, GPIO_MODE_OUTPUT);
    gpio_set_direction(UI_WHITE_1_3MM, GPIO_MODE_OUTPUT);
    xTaskCreate(&blinkTask, "blink_task", 2048, NULL, 7, NULL);
    espRc = ESP_OK;
#endif
    return espRc;
}

esp_err_t uiShow(int state, esp_err_t error) {
    ESP_LOGI(TAG, "uiShow %d 0x%X", state, error);
    esp_err_t espRc;
#ifdef UI_I2C_ADDRESS
    if(uiOk) {
        if(error) {
            snprintf(text, sizeof(text), "  %s %04X", pafStateTexts[state], error);
        } else {
            snprintf(text, sizeof(text), "  %s     ", pafStateTexts[state]);
        }
        uint8_t textLen = strlen(text);
        ESP_LOGI(TAG, "uiShow '%s' %d", text, textLen);
        
	uint8_t cur_page = 3;

        xSemaphoreTake(semI2c, portMAX_DELAY);
        
        if(! uiCleared) {
            memset(zero, 0, sizeof(zero));
            espRc = ESP_OK;
            for (uint8_t i = 0; espRc == ESP_OK && i < 8; i++) {
                i2c_cmd_handle_t cmd = i2c_cmd_link_create();
                i2c_master_start(cmd);
                i2c_master_write_byte(cmd, (UI_I2C_ADDRESS << 1) | I2C_MASTER_WRITE, true);
                i2c_master_write_byte(cmd, OLED_CONTROL_BYTE_CMD_SINGLE, true);
                i2c_master_write_byte(cmd, 0xB0 | i, true);

                i2c_master_write_byte(cmd, OLED_CONTROL_BYTE_DATA_STREAM, true);
                i2c_master_write(cmd, zero, 128, true);
                i2c_master_stop(cmd);
                espRc = i2c_master_cmd_begin(uiI2cPort, cmd, 10 / portTICK_PERIOD_MS);
                i2c_cmd_link_delete(cmd);
            }
            ESP_LOGI(TAG, "OLED clear result code: 0x%.2X", espRc);
            vTaskDelay(100 / portTICK_PERIOD_MS);
//            uiCleared = espRc == ESP_OK;
        }
        
        i2c_cmd_handle_t cmd = i2c_cmd_link_create();
        i2c_master_start(cmd);
        i2c_master_write_byte(cmd, (UI_I2C_ADDRESS << 1) | I2C_MASTER_WRITE, true);

        i2c_master_write_byte(cmd, OLED_CONTROL_BYTE_CMD_STREAM, true);
        i2c_master_write_byte(cmd, 0x00, true); // reset column
        i2c_master_write_byte(cmd, 0x10, true);
        i2c_master_write_byte(cmd, 0xB0 | cur_page, true); // reset page

        i2c_master_stop(cmd);
        espRc = i2c_master_cmd_begin(uiI2cPort, cmd, 10 / portTICK_PERIOD_MS);
        i2c_cmd_link_delete(cmd);

        for (uint8_t i = 0; espRc == ESP_OK && i < textLen; i++) {
            cmd = i2c_cmd_link_create();
            i2c_master_start(cmd);
            i2c_master_write_byte(cmd, (UI_I2C_ADDRESS << 1) | I2C_MASTER_WRITE, true);

            i2c_master_write_byte(cmd, OLED_CONTROL_BYTE_DATA_STREAM, true);
            i2c_master_write(cmd, font8x8_basic_tr[(uint8_t) text[i]], 8, true);

            i2c_master_stop(cmd);
            espRc = i2c_master_cmd_begin(uiI2cPort, cmd, 10 / portTICK_PERIOD_MS);
            i2c_cmd_link_delete(cmd);
        }
        
        xSemaphoreGive(semI2c);
        ESP_LOGI(TAG, "uiShow 0x%X", espRc);
    } else {
        espRc = ESP_FAIL;
    }
#endif
#ifdef UI_RED_PIN
    curState = state;
    curError = error;
    espRc = ESP_OK;
#endif
#ifdef UI_WHITE_0_3MM
    espRc = ESP_OK;
#endif
    return espRc;
}
