/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2020 by Daniel Quintela
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files 
 * (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, 
 * publish, distribute, sublicense, and/or sell copies of the Software, 
 * and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be 
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY 
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/* 
 * File:   paf.h
 * Author: danielq
 *
 * Created on 11 de enero de 2020, 09:57
 */

#ifndef PAF_H
#define PAF_H

// Esp32 board selection
#define AERK_DOIT_DEVKIT  1
#define AERK_WEMOS_BAT    2
#define AERK_WROVER_KIT   3
#define AERK_DOIT_DVKT_30 4

#define AERK_BOARD        AERK_DOIT_DVKT_30


#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "driver/i2c.h"
#include "driver/uart.h"
#include <math.h>
#include <string.h>

#include <nvs_flash.h>
#include <esp_log.h>

#if AERK_BOARD == AERK_DOIT_DEVKIT
//  DOIT dev kit V1 - 36 pins
#   define I2C_MASTER_SCL_IO          GPIO_NUM_22        /*!< gpio number for I2C master clock */
#   define I2C_MASTER_SDA_IO          GPIO_NUM_21        /*!< gpio number for I2C master data  */
#   define I2C_MASTER_NUM             I2C_NUM_0          /*!< I2C port number for master dev */
#   define I2C_MASTER_FREQ_HZ         100000             /*!< I2C master clock frequency */
#   define I2C_MASTER_TX_BUF_DISABLE  0                  /*!< I2C master doesn't need buffer */
#   define I2C_MASTER_RX_BUF_DISABLE  0                  /*!< I2C master doesn't need buffer */
#   define GPS_UART_NUM               UART_NUM_2
#   define GPS_TXD_PIN                (GPIO_NUM_16)
#   define GPS_RXD_PIN                (GPIO_NUM_17)
#   define PMS_SECONDS_FOR_OPEN       120
#   define PMS_SECONDS_FOR_READ       150
//  PMS5003 via serial 1
#   define PMS_UART_NUM               UART_NUM_1
#   define PMS_TXD_PIN                (GPIO_NUM_4)
#   define PMS_RXD_PIN                (GPIO_NUM_2)
//  UI via display I2C
#   define UI_I2C_ADDRESS             0x3C
#elif AERK_BOARD == AERK_WEMOS_BAT
//  Wemos with battery holder
#   define I2C_MASTER_SCL_IO          GPIO_NUM_21        /*!< gpio number for I2C master clock */
#   define I2C_MASTER_SDA_IO          GPIO_NUM_22        /*!< gpio number for I2C master data  */
#   define I2C_MASTER_NUM             I2C_NUM_0          /*!< I2C port number for master dev */
#   define I2C_MASTER_FREQ_HZ         100000             /*!< I2C master clock frequency */
#   define I2C_MASTER_TX_BUF_DISABLE  0                  /*!< I2C master doesn't need buffer */
#   define I2C_MASTER_RX_BUF_DISABLE  0                  /*!< I2C master doesn't need buffer */
#   define GPS_UART_NUM               UART_NUM_2
#   define GPS_TXD_PIN                (GPIO_NUM_4)
#   define GPS_RXD_PIN                (GPIO_NUM_2)
#   define PMS_SECONDS_FOR_OPEN       120
#   define PMS_SECONDS_FOR_READ       150
//  PMSA003 via I2C and SET pin
#   define I2C_PMS_ADDRESS            0x12
#   define PMS_SET_GPIO               GPIO_NUM_32
//  UI via tri-color LED
#   define UI_RED_PIN                 GPIO_NUM_25
#   define UI_GREEN_PIN               GPIO_NUM_27
#   define UI_BLUE_PIN                GPIO_NUM_26
#elif AERK_BOARD == AERK_WROVER_KIT
//  ESP-WROVER-KIT V3
#   define I2C_MASTER_SCL_IO          GPIO_NUM_22        /*!< gpio number for I2C master clock */
#   define I2C_MASTER_SDA_IO          GPIO_NUM_21        /*!< gpio number for I2C master data  */
#   define I2C_MASTER_NUM             I2C_NUM_0          /*!< I2C port number for master dev */
#   define I2C_MASTER_FREQ_HZ         100000             /*!< I2C master clock frequency */
#   define I2C_MASTER_TX_BUF_DISABLE  0                  /*!< I2C master doesn't need buffer */
#   define I2C_MASTER_RX_BUF_DISABLE  0                  /*!< I2C master doesn't need buffer */
#   define GPS_UART_NUM               UART_NUM_2
#   define GPS_TXD_PIN                (GPIO_NUM_26)
#   define GPS_RXD_PIN                (GPIO_NUM_27)
#   define PMS_SECONDS_FOR_OPEN       120
#   define PMS_SECONDS_FOR_READ       150
//  PMS5003 via serial 1
#   define PMS_UART_NUM               UART_NUM_1
#   define PMS_TXD_PIN                (GPIO_NUM_35)
#   define PMS_RXD_PIN                (GPIO_NUM_34)
//  UI via tri-color LED
#   define UI_RED_PIN                 GPIO_NUM_0
#   define UI_GREEN_PIN               GPIO_NUM_2
#   define UI_BLUE_PIN                GPIO_NUM_4
#elif AERK_BOARD == AERK_DOIT_DVKT_30
//  DOIT dev kit V1 - 30 pins
#   define I2C_MASTER_SCL_IO          GPIO_NUM_22        /*!< gpio number for I2C master clock */
#   define I2C_MASTER_SDA_IO          GPIO_NUM_21        /*!< gpio number for I2C master data  */
#   define I2C_MASTER_NUM             I2C_NUM_0          /*!< I2C port number for master dev */
#   define I2C_MASTER_FREQ_HZ         100000             /*!< I2C master clock frequency */
#   define I2C_MASTER_TX_BUF_DISABLE  0                  /*!< I2C master doesn't need buffer */
#   define I2C_MASTER_RX_BUF_DISABLE  0                  /*!< I2C master doesn't need buffer */
#   define GPS_UART_NUM               UART_NUM_2
#   define GPS_TXD_PIN                (GPIO_NUM_13)
#   define GPS_RXD_PIN                (GPIO_NUM_27)
#   define PMS_SECONDS_FOR_OPEN       120
#   define PMS_SECONDS_FOR_READ       150
//  PMS5003 via serial 1
#   define PMS_UART_NUM               UART_NUM_1
#   define PMS_TXD_PIN                (GPIO_NUM_16)
#   define PMS_RXD_PIN                (GPIO_NUM_17)
//  UI via one bi-color LED and two white LEDs
#   define UI_WHITE_0_3MM             GPIO_NUM_26
#   define UI_RED_5MM                 GPIO_NUM_25
#   define UI_GREEN_5MM               GPIO_NUM_33
#   define UI_WHITE_1_3MM             GPIO_NUM_32
#   define UI_AP_START_BUTTON         GPIO_NUM_36
//  Uses BME680 instead of BME280+CCS811
#   define PAF_AP_INACT_TIME          180
#   define PAF_HAS_BME680
#endif
#define PMS_FRAME_SIZE            32

//#   define AERK_PROXY_URL "https://soongsoft.com/aerkproxy/telemetry"
#   define AERK_PROXY_URL "http://soongsoft.com/aerkproxy/telemetry"
//#   define AERK_PROXY_URL "https://11.0.0.244/aerkproxy/telemetry"

#define HTE_QUEUE_LENGTH 256
#define HTE_HAS_PM       1
#define HTE_HAS_TVOC     2
#define HTE_IS_A_MESSAGE 0x80

#define PAF_TELEMETRY_CMD           0
#define PAF_TELEMETRY_MSG           1

#define PAF_TELEMETRY_CMD_DISC      0
#define PAF_TELEMETRY_CMD_CONN      1

#define PAF_STATE_BOOTING           0
#define PAF_STATE_CONNECTING        1
#define PAF_STATE_NOT_CONNECTED     2
#define PAF_STATE_CONNECTED         3
#define PAF_STATE_IDLE              4
#define PAF_STATE_READING           5

#define HTE_MSG_CF_STARTING         0
#define HTE_MSG_CCS_REINIT          1

typedef enum {
    MQTT_OFF = 0,
    MQTT_ON
} telemetryCmds;

struct HbbTelemetryEntry {
    uint8_t flags;
    uint8_t iMsg;
    int64_t ts;
    float lat;
    float lon;
    float t;
    float h;
    float p;
    uint16_t pm10;
    uint16_t pm25;
    uint16_t pm100;
    uint16_t tvoc;
    uint16_t co2;
};


esp_err_t commsInit(void);
void commsTask(void* param);
esp_err_t commsPublishData(struct HbbTelemetryEntry * pHte);
esp_err_t commsPublishMessage(const uint8_t iMsg);

esp_err_t telemetryInit(void);
void telemetryTask(void* param);
void telemetryTaskCmd(uint8_t cmd);
void telemetryTaskMsg(uint8_t type, uint32_t data);

esp_err_t positionInit(void);
void positionTask(void* param);
esp_err_t setEntryPosition(struct HbbTelemetryEntry *pHte);

esp_err_t uiInit(i2c_port_t port, SemaphoreHandle_t sem);
esp_err_t uiShow(int state, esp_err_t error);

extern QueueHandle_t qTelemetry;
extern EventGroupHandle_t egComms;

extern bool commsHasIp;
extern bool commsHasAp;
extern bool positionFixed;

#endif /* PAF_H */

