/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2020 by Daniel Quintela
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files 
 * (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, 
 * publish, distribute, sublicense, and/or sell copies of the Software, 
 * and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be 
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY 
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/* 
 * File:   comms.c
 * Author: danielq
 *
 * Created on 11 de enero de 2020, 10:06
 */

#include "paf.h"

#include <esp_wifi.h>
#include <esp_event_loop.h>
#include <esp_http_server.h>
#include <esp_http_client.h>
#include "lwip/apps/sntp.h"

static int calculateIAQScore(float t, float h, float g) {
    int score = 0;
    if(t < 21.0) {
        score += (21.0 - t) * 0.243902439;
    } else {
        score += (t - 21) * 0.126582278;
    }
    if(h < 40.0) {
        score += (40 - h) * 0.25;
    } else {
        score += (h - 40) * 0.166666667;
    }
    if(g < 50.0) {
        score += 80;
    } else if(g < 50000.0) {
        score += (50000.0 - g) * 0.001601602;
    }
    return score;
}

bool commsHasIp;
bool commsHasAp;

static const char *TAG = "comms";
static const char* cfgForm =
"<!DOCTYPE HTML>"
"<html>"
"<head>"
"<meta charset=\"UTF-8\">"
"<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">"
"<title>New&nbsp;configuration</title>"
"</head>"
"<body>"
"<div style=\"border-style:solid;border-width:1px;border-color:black;padding-left:10px\">"
"<form action=\"\" method=\"POST\">"
"<br>"
"<div>"
"<label for=\"ssi0\">SSID&nbsp;0</label>"
"&nbsp;"
"<input type=\"text\" id=\"ssi0\" name=\"ssi0\" value=\"%s\">"
"</div>"
"<br>"
"<div>"
"<label for=\"pas0\">Password&nbsp;0</label>"
"&nbsp;"
"<input type=\"password\" id=\"pas0\" name=\"pas0\" value=\"%s\">"
"</div>"
"<br>"
"<div>"
"<label for=\"ssi1\">SSID&nbsp;1</label>"
"&nbsp;"
"<input type=\"text\" id=\"ssi1\" name=\"ssi1\" value=\"%s\">"
"</div>"
"<br>"
"<div>"
"<label for=\"pas1\">Password&nbsp;</label>"
"&nbsp;"
"<input type=\"password\" id=\"pas1\" name=\"pas1\" value=\"%s\">"
"</div>"
"<br>"
"<div>"
"<label for=\"apply\">New&nbsp;configuration</label>"
"&nbsp;"
"<input type=\"submit\" id=\"apply\" value=\"Apply\">"
"</div>"
"<br>"
"</form>"
"</div>"
"</body>"
"</html>"
;
static char myName[20];
static char frmBuf[2048];
static char wifiSsiz[2][32];
static char wifiPasz[2][64];
/* static */ int idxSsid;
static uint8_t curSsid[32];
static uint8_t curPass[64];
static QueueHandle_t hteQueue;
static char pubBuf[1024];
static int wiFiStationStatus;

#ifdef PAF_AP_INACT_TIME
#include <time.h>
static time_t apLastActivity;
#endif

const char * HTE_MSGS[] = {
    "CF with PAF is starting comms",
    "CCS811 suspicious behavior: reinit it"
};

static esp_err_t loadWifiInfo() {
    nvs_handle nvsh;
    size_t lSsiz = sizeof(wifiSsiz);
    size_t lPasz = sizeof(wifiPasz);
    esp_err_t ee = nvs_open("maf", NVS_READONLY, &nvsh);
    if(ee == ESP_OK) {
        ee = nvs_get_blob(nvsh, "ssiz", wifiSsiz, &lSsiz);
        if(ee == ESP_OK) {
            ee = nvs_get_blob(nvsh, "pasz", wifiPasz, &lPasz);
        }
        nvs_close(nvsh);
    }
    ESP_LOGI(TAG, "loadWifiInfo 0x%X", ee);
    if(ee != ESP_OK) {
        wifiSsiz[0][0] = 0;
        wifiSsiz[1][0] = 0;
    }
    return ee;
}

static esp_err_t saveWifiInfo() {
    nvs_handle nvsh;
    size_t lSsiz = sizeof(wifiSsiz);
    size_t lPasz = sizeof(wifiPasz);
    esp_err_t ee = nvs_open("maf", NVS_READWRITE, &nvsh);
    if(ee == ESP_OK) {
        ee = nvs_set_blob(nvsh, "ssiz", wifiSsiz, lSsiz);
        if(ee == ESP_OK) {
            ee = nvs_set_blob(nvsh, "pasz", wifiPasz, lPasz);
            if(ee == ESP_OK) {
                ee = nvs_commit(nvsh);
            }
        }
        nvs_close(nvsh);
    }
    ESP_LOGI(TAG, "saveWifiInfo 0x%X", ee);
    return ee;
}

esp_err_t getSlashHandler(httpd_req_t *req) {
    apLastActivity = time(NULL);
    snprintf(frmBuf, sizeof(frmBuf), cfgForm, wifiSsiz[0], wifiPasz[0], wifiSsiz[1], wifiPasz[1]);
    httpd_resp_send(req, frmBuf, strlen(frmBuf));
    return ESP_OK;
}

httpd_uri_t getSlash = {
    .uri       = "/aerk",
    .method    = HTTP_GET,
    .handler   = getSlashHandler,
    .user_ctx  = NULL
};

static void trm(char * txt) {
    for(int j = strlen(txt) - 1; j >= 0; j--) {
        if(txt[j] == ' ') {
            txt[j] = 0;
        } else {
            break;
        }
    }
}

esp_err_t postSlashHandler(httpd_req_t *req) {
    int ret, remaining = req->content_len;
    char * buf = malloc(remaining + 3);
    char * pbuf = buf + 1;
    
    apLastActivity = time(NULL);
    while (remaining > 0) {
        /* Read the data for the request */
        if ((ret = httpd_req_recv(req, pbuf,
                        remaining)) <= 0) {
            if (ret == HTTPD_SOCK_ERR_TIMEOUT) {
                /* Retry receiving if timeout occurred */
                continue;
            }
            return ESP_FAIL;
        }
        remaining -= ret;
    }   
    buf[0] = '&';
    int cs = 1;
    int cd = 1;
    while(cs <= req->content_len) {
        if(buf[cs] == '%') {
            cs++;
            buf[cd] = (buf[cs] > '9' ? buf[cs] - 55 : buf[cs] - 48) << 4;
            cs++;
            buf[cd] |= (buf[cs] > '9' ? buf[cs] - 55 : buf[cs] - 48) & 0xF;
        } else if(buf[cs] == '+') {
            buf[cd] = ' ';
        } else {
            buf[cd] = buf[cs];
        }
        cs++;
        cd++;
    }
    buf[cd++] = '&';
    buf[cd++] = 0;
    ESP_LOGI(TAG, "postSlashHandler '%s'", buf);    
    
    char *ssi0;
    char *pas0;
    char *ssi1;
    char *pas1;
    bool ok = false;
    ssi0 = strstr(buf, "&ssi0=");
    if(ssi0 != NULL) {
        pas0 = strstr(buf, "&pas0=");
        if(pas0 != NULL) {
            ssi1 = strstr(buf, "&ssi1=");
            if(ssi1 != NULL) {
                pas1 = strstr(buf, "&pas1=");
                if(pas1 != NULL) {
                    pbuf = strstr(ssi0 + 1, "&");
                    if(pbuf != NULL) {
                        *pbuf = 0;
                        pbuf = strstr(pas0 + 1, "&");
                        if(pbuf != NULL) {
                            *pbuf = 0;
                            
                            pbuf = strstr(ssi1 + 1, "&");
                            if(pbuf != NULL) {
                                *pbuf = 0;
                                pbuf = strstr(pas1 + 1, "&");
                                if(pbuf != NULL) {
                                    *pbuf = 0;
                                    ssi0 += 6;
                                    pas0 += 6;
                                    ssi1 += 6;
                                    pas1 += 6;
                                    ok = true;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    httpd_resp_set_hdr(req, "Location", "/aerk");
    httpd_resp_set_status(req, "303 See Other");
    httpd_resp_set_type(req, HTTPD_TYPE_TEXT);
    httpd_resp_send(req, NULL, 0);
    ESP_LOGI(TAG, "postSlashHandler ok = %d", ok);   
    if(ok) {
        trm(ssi0);
        trm(pas0);
        trm(ssi1);
        trm(pas1);
        strlcpy(wifiSsiz[0], ssi0, 32);
        strlcpy(wifiPasz[0], pas0, 64);
        strlcpy(wifiSsiz[1], ssi1, 32);
        strlcpy(wifiPasz[1], pas1, 64);
        idxSsid = 0;
        strlcpy((char *) curSsid, wifiSsiz[0], sizeof(curSsid));
        strlcpy((char *) curPass, wifiPasz[0], sizeof(curPass));
        esp_err_t ee = esp_wifi_disconnect();
        ESP_LOGI(TAG, "postSlashHandler esp_wifi_disconnect 0x%X", ee);
        vTaskDelay(2000 / portTICK_PERIOD_MS);
        wifi_config_t wifiConfig = {
            .sta = {
                .ssid = "",
                .password = "",
            },
        };
        strlcpy((char *) wifiConfig.sta.ssid, (char *) curSsid, sizeof(curSsid));
        strlcpy((char *) wifiConfig.sta.password, (char *) curPass, sizeof(curPass));
        saveWifiInfo();
        ESP_LOGI(TAG, "Setting WiFi configuration SSID '%s' (%s)...", wifiConfig.sta.ssid, wifiConfig.sta.password);
        ee = esp_wifi_set_config(ESP_IF_WIFI_STA, &wifiConfig);
        ESP_LOGI(TAG, "postSlashHandler esp_wifi_set_config 0x%X", ee);   
        telemetryTaskMsg(PAF_STATE_CONNECTING, idxSsid + 1);
        ee = esp_wifi_connect();
        ESP_LOGI(TAG, "postSlashHandler esp_wifi_disconnect 0x%X", ee);   
    }
    return ESP_OK;
}

httpd_uri_t postSlash = {
    .uri       = "/aerk",
    .method    = HTTP_POST,
    .handler   = postSlashHandler,
    .user_ctx  = NULL
};



httpd_handle_t startWebserver(void) {
    httpd_handle_t server = NULL;
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();

    // Start the httpd server
    ESP_LOGI(TAG, "Starting server on port: '%d'", config.server_port);
    if (httpd_start(&server, &config) == ESP_OK) {
        // Set URI handlers
        ESP_LOGI(TAG, "Registering URI handlers");
        httpd_register_uri_handler(server, &getSlash);
        httpd_register_uri_handler(server, &postSlash);
        return server;
    }

    ESP_LOGI(TAG, "Error starting server!");
    return NULL;
}

void stopWebserver(httpd_handle_t server) {
    // Stop the httpd server
    httpd_stop(server);
}

static esp_err_t wifiEventHandler(void *ctx, system_event_t *event) {
    httpd_handle_t *server = (httpd_handle_t *) ctx;
    /* For accessing reason codes in case of disconnection */
    system_event_info_t *info = &event->event_info;
    esp_err_t ee;

    switch(event->event_id) {
        case SYSTEM_EVENT_AP_START:
            commsHasAp = true;
            ESP_LOGI(TAG, "SYSTEM_EVENT_AP_START");
            if (*server == NULL) {
                *server = startWebserver();
            }
            break;
        case SYSTEM_EVENT_AP_STOP:
            commsHasAp = false;
            ESP_LOGI(TAG, "SYSTEM_EVENT_AP_STOP");
            if (*server) {
                stopWebserver(*server);
                *server = NULL;
            }
            break;
    case SYSTEM_EVENT_STA_START:
        ESP_LOGI(TAG, "SYSTEM_EVENT_STA_START");
        wiFiStationStatus = 1;
        ee = esp_wifi_connect();
        ESP_LOGI(TAG, "esp_wifi_connect 0x%X", ee);
        if(ee == ESP_OK) {
            wiFiStationStatus = 2;
        }
        break;
    case SYSTEM_EVENT_STA_GOT_IP:
        ESP_LOGI(TAG, "SYSTEM_EVENT_STA_GOT_IP");
        ESP_LOGI(TAG, "Got IP: '%s'",
                ip4addr_ntoa(&event->event_info.got_ip.ip_info.ip));
        xEventGroupSetBits(egComms, BIT1);
        break;
    case SYSTEM_EVENT_STA_DISCONNECTED:
        wiFiStationStatus = 1;
        xEventGroupSetBits(egComms, BIT0);
        ESP_LOGI(TAG, "SYSTEM_EVENT_STA_DISCONNECTED");
        ESP_LOGE(TAG, "Disconnect reason : %d", info->disconnected.reason);
        int nidx = idxSsid ? 0 : 1;
        if(wifiSsiz[nidx][0]) {
            strlcpy((char *) curSsid, wifiSsiz[nidx], sizeof(curSsid));
            strlcpy((char *) curPass, wifiPasz[nidx], sizeof(curPass));
            wifi_config_t wifiConfig = {
                .sta = {
                    .ssid = "",
                    .password = "",
                },
            };
            strlcpy((char *) wifiConfig.sta.ssid, (char *) curSsid, sizeof(curSsid));
            strlcpy((char *) wifiConfig.sta.password, (char *) curPass, sizeof(curPass));        
            ee = esp_wifi_set_config(ESP_IF_WIFI_STA, &wifiConfig);
            if(ee == ESP_OK) {
                telemetryTaskMsg(PAF_STATE_CONNECTING, nidx + 1);
                ee = esp_wifi_connect();
                if(ee == ESP_OK) {
                    idxSsid = nidx;
                }
            }
            if(ee != ESP_OK) {
                strlcpy((char *) curSsid, wifiSsiz[idxSsid], sizeof(curSsid));
                strlcpy((char *) curPass, wifiPasz[idxSsid], sizeof(curPass));
                ee = esp_wifi_set_config(ESP_IF_WIFI_STA, &wifiConfig);
                ESP_LOGI(TAG, "esp_wifi_set_config 0x%X", ee);
                telemetryTaskMsg(PAF_STATE_CONNECTING, idxSsid + 1);
                ee = esp_wifi_connect();
                ESP_LOGI(TAG, "esp_wifi_connect 0x%X", ee);
                if(ee == ESP_OK) {
                    wiFiStationStatus = 2;
                }
            }
        } else {
            ee = esp_wifi_connect();
            ESP_LOGI(TAG, "esp_wifi_connect 0x%X", ee);
            if(ee == ESP_OK) {
                wiFiStationStatus = 2;
            }
        }
        break;
    default:
        break;
    }
    return ESP_OK;
}

static esp_err_t http_client_event_handle(esp_http_client_event_t *evt) {
    char * buf;
    switch(evt->event_id) {
        case HTTP_EVENT_ERROR:
            ESP_LOGI(TAG, "HTTP_EVENT_ERROR");
            break;
        case HTTP_EVENT_ON_CONNECTED:
            ESP_LOGI(TAG, "HTTP_EVENT_ON_CONNECTED");
            break;
        case HTTP_EVENT_HEADER_SENT:
            ESP_LOGI(TAG, "HTTP_EVENT_HEADER_SENT");
            break;
        case HTTP_EVENT_ON_HEADER:
            ESP_LOGI(TAG, "HTTP_EVENT_ON_HEADER");
            buf = malloc(evt->data_len + 1);
            memcpy(buf, evt->data, evt->data_len);
            buf[evt->data_len] = 0;
            ESP_LOGI(TAG, "%d '%s'", evt->data_len, buf);
            free(buf);
            break;
        case HTTP_EVENT_ON_DATA:
            ESP_LOGI(TAG, "HTTP_EVENT_ON_DATA, len=%d", evt->data_len);
            if (!esp_http_client_is_chunked_response(evt->client)) {
                buf = malloc(evt->data_len + 1);
                memcpy(buf, evt->data, evt->data_len);
                buf[evt->data_len] = 0;
                ESP_LOGI(TAG, "%d '%s'", evt->data_len, buf);
                free(buf);
            }

            break;
        case HTTP_EVENT_ON_FINISH:
            ESP_LOGI(TAG, "HTTP_EVENT_ON_FINISH");
            break;
        case HTTP_EVENT_DISCONNECTED:
            ESP_LOGI(TAG, "HTTP_EVENT_DISCONNECTED");
            break;
    }
    return ESP_OK;
}

static esp_err_t commsWiFiStart(bool withAp) {
    esp_err_t ee = esp_wifi_set_mode(withAp ? WIFI_MODE_APSTA : WIFI_MODE_STA);
    ESP_LOGI(TAG, "commsWiFiStart esp_wifi_set_mode 0x%X", ee);
    wifi_config_t wifiConfig = {};
    strlcpy((char *) wifiConfig.sta.ssid, (char *) curSsid, sizeof(wifiConfig.sta.ssid));
    strlcpy((char *) wifiConfig.sta.password, (char *) curPass, sizeof(wifiConfig.sta.password));
    ee = esp_wifi_set_config(ESP_IF_WIFI_STA, &wifiConfig);
    ESP_LOGI(TAG, "commsWiFiStart esp_wifi_set_config STA 0x%X", ee);
    if(withAp) {
        wifi_config_t apConfig = {
            .ap = {
                .authmode = WIFI_AUTH_OPEN,
                .beacon_interval = 100,
                .channel = 6,
                .max_connection = 4,
                .password = "",
                .ssid = "",
                .ssid_hidden = 0,
                .ssid_len = 0
            }
        };
        strncpy((char *) apConfig.ap.ssid, myName, strlen(myName));
        ee = esp_wifi_set_config(ESP_IF_WIFI_AP, &apConfig);
        ESP_LOGI(TAG, "commsWiFiStart esp_wifi_set_config AP 0x%X", ee);
    }
    ee = esp_wifi_start();
    ESP_LOGI(TAG, "commsWiFiStart esp_wifi_start 0x%X", ee);
    return ESP_OK;
}

esp_err_t commsInit(void) {
    uint8_t mac[6];
    esp_efuse_mac_get_default(mac);
    snprintf(myName, 20, "CF32-%02X%02X%02X%02X%02X%02X", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
    ESP_LOGI(TAG, "start %s", myName);
    
    hteQueue = xQueueCreate(HTE_QUEUE_LENGTH, sizeof(struct HbbTelemetryEntry));    
    
    wiFiStationStatus = 0;
    commsHasAp = false;
    apLastActivity = 0;
    
    static httpd_handle_t server = NULL;
    tcpip_adapter_init();
    ESP_ERROR_CHECK(esp_event_loop_init(wifiEventHandler, &server));
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));
    ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM));
    
    nvs_handle nvsh;
    esp_err_t ee = nvs_open("maf", NVS_READONLY, &nvsh);
    ESP_LOGI(TAG, "nvs_open 0x%X", ee);
    if(ee == ESP_OK) {
        char sspp[32];
        size_t spl = 32;
        ee = nvs_get_str(nvsh, "ssid", sspp, &spl);
        ESP_LOGI(TAG, "nvs_get_str %d", ee);
        nvs_close(nvsh);
        ESP_LOGI(TAG, "nvs_close");
    }
    
    ee = loadWifiInfo();
    if(ee != ESP_OK) {
        ESP_LOGI(TAG, "No configuration on NVS");
        strlcpy(wifiSsiz[0], "kepler10", 32);
        strlcpy(wifiPasz[0], "!0+2020==", 64);
        wifiSsiz[1][0] = 0;
        wifiPasz[1][0] = 0;
    }
    idxSsid = 0;
    strlcpy((char *) curSsid, wifiSsiz[0], sizeof(curSsid));
    strlcpy((char *) curPass, wifiPasz[0], sizeof(curPass));
    ee = commsWiFiStart(false);
    
    return ESP_OK;
}

void commsTask(void* param) {
    commsHasIp = false;
    TickType_t tt = 1000 / portTICK_PERIOD_MS;
    while(true) {
        EventBits_t bits = xEventGroupWaitBits(egComms, BIT0 | BIT1 
#ifdef PAF_AP_INACT_TIME
                            | BIT2
#endif
                            ,pdTRUE, pdFALSE, tt);
        if(bits) {
            if(bits & BIT0) {
                if(commsHasIp) {
                    telemetryTaskCmd(PAF_TELEMETRY_CMD_DISC);
                    sntp_stop();
                    commsHasIp = false;
                    tt = 1000 / portTICK_PERIOD_MS;
                }
            }
            if(bits & BIT1) {
                if(!commsHasIp) {
                    commsHasIp = true;
                    ESP_LOGI(TAG, "Initializing SNTP");
                    sntp_setoperatingmode(SNTP_OPMODE_POLL);
                    sntp_setservername(0, "pool.ntp.org");
                    sntp_init();
                    // wait for time to be set
                    time_t now = 0;
                    struct tm timeinfo = { 0 };
                    int retry = 0;
                    const int retryCount = 10;
                    while (timeinfo.tm_year < (2016 - 1900) && ++retry < retryCount) {
                        ESP_LOGI(TAG, "Waiting for system time to be set... (%d/%d)", retry, retryCount);
                        vTaskDelay(2000 / portTICK_PERIOD_MS);
                        time(&now);
                        localtime_r(&now, &timeinfo);
                        if(timeinfo.tm_year > (2016 - 1900)) {
                            commsPublishMessage(HTE_MSG_CF_STARTING);
                        }
                    }
                    telemetryTaskCmd(PAF_TELEMETRY_CMD_CONN);
                }
            }
#ifdef PAF_AP_INACT_TIME
            if(bits & BIT2) {
                // activated ap button
                if(! apLastActivity) {
                    apLastActivity = time(NULL);
                    esp_err_t ee = esp_wifi_stop();
                    ESP_LOGI(TAG, "esp_wifi_stop 0x%X", ee);
                    vTaskDelay(1000 / portTICK_PERIOD_MS);
                    ee = commsWiFiStart(true);
                    ESP_LOGI(TAG, "commsWiFiStart 0x%X", ee);
                }
            }
#endif
        } else {
            if(apLastActivity) {
                time_t tnow = time(NULL);
                ESP_LOGI(TAG, "ap inact time control %ld - %ld = %ld", tnow, apLastActivity, tnow - apLastActivity);
                if((tnow - apLastActivity) >= PAF_AP_INACT_TIME) {
                    esp_err_t ee = esp_wifi_stop();
                    ESP_LOGI(TAG, "esp_wifi_stop 0x%X", ee);
                    vTaskDelay(1000 / portTICK_PERIOD_MS);
                    ee = commsWiFiStart(false);
                    ESP_LOGI(TAG, "commsWiFiStart 0x%X", ee);
                    vTaskDelay(1000 / portTICK_PERIOD_MS);
                    apLastActivity = 0;
                }
            }
            if(commsHasIp) {
                struct HbbTelemetryEntry hte;
                if(xQueuePeek(hteQueue, &hte, (TickType_t) 0) == pdTRUE) {
                    if(hte.flags & HTE_IS_A_MESSAGE) { // is amessage
                        snprintf(pubBuf, sizeof(pubBuf), 
                                "{\"ts\":%lld,\"values\":{"
                                "\"message\":\"%s\"}}",
                                hte.ts,
                                HTE_MSGS[hte.iMsg]
                                );
                    } else if(hte.flags & HTE_HAS_PM) { // has PM
                        if(hte.flags & HTE_HAS_TVOC) { // has TVOC
                            snprintf(pubBuf, sizeof(pubBuf), 
                                    "{\"ts\":%lld,\"values\":{"
                                    "\"lat\":%f,\"lon\":%f,"
                                    "\"t\":%.1f,\"h\":%.1f,\"p\":%.1f,"
#ifdef PAF_HAS_BME680
                                    "\"gasres\":%d,\"iaq\":%d,"
#else
                                    "\"eco2\":%d,\"voc\":%d,"
#endif
                                    "\"pm10\":%d,\"pm25\":%d,\"pm100\":%d}}",
                                    hte.ts,
                                    hte.lat, hte.lon,
                                    hte.t, hte.h, hte.p,
#ifdef PAF_HAS_BME680
                                    hte.tvoc, calculateIAQScore(hte.t, hte.h, hte.tvoc * 1.0),
#else
                                    hte.co2, hte.tvoc,
#endif
                                    hte.pm10, hte.pm25, hte.pm100
                                    );
                        } else {
                            snprintf(pubBuf, sizeof(pubBuf), 
                                    "{\"ts\":%lld,\"values\":{"
                                    "\"lat\":%f,\"lon\":%f,"
                                    "\"t\":%.1f,\"h\":%.1f,\"p\":%.1f,"
                                    "\"pm10\":%d,\"pm25\":%d,\"pm100\":%d}}",
                                    hte.ts,
                                    hte.lat, hte.lon,
                                    hte.t, hte.h, hte.p,
                                    hte.pm10, hte.pm25, hte.pm100
                                    );
                        }
                    } else {
                        if(hte.flags & HTE_HAS_TVOC) { // has TVOC
                            snprintf(pubBuf, sizeof(pubBuf), 
                                    "{\"ts\":%lld,\"values\":{"
                                    "\"lat\":%f,\"lon\":%f,"
                                    "\"t\":%.1f,\"h\":%.1f,\"p\":%.1f,"
#ifdef PAF_HAS_BME680
                                    "\"gasres\":%d,\"iaq\":%d}}",
#else
                                    "\"eco2\":%d,\"voc\":%d}}",
#endif
                                    hte.ts,
                                    hte.lat, hte.lon,
                                    hte.t, hte.h, hte.p,
#ifdef PAF_HAS_BME680
                                    hte.tvoc, calculateIAQScore(hte.t, hte.h, hte.tvoc * 1.0)
#else
                                    hte.co2, hte.tvoc
#endif
                                    );
                        } else {
                            snprintf(pubBuf, sizeof(pubBuf), 
                                    "{\"ts\":%lld,\"values\":{"
                                    "\"lat\":%f,\"lon\":%f,"
                                    "\"t\":%.1f,\"h\":%.1f,\"p\":%.1f}}",
                                    hte.ts,
                                    hte.lat, hte.lon,
                                    hte.t, hte.h, hte.p
                                    );
                        }
                    }
                    ESP_LOGI(TAG, "pubBuf '%s'", pubBuf);
                    esp_http_client_config_t config = {
                        .url = AERK_PROXY_URL,
                        .event_handler = http_client_event_handle,
                        .method = HTTP_METHOD_POST,
                        .cert_pem = NULL,
                    };
                    esp_http_client_handle_t client = esp_http_client_init(&config);
                    esp_http_client_set_header(client, "X-AERK-ID", myName);
                    esp_http_client_set_header(client, "Content-Type", "application/json");
                    esp_http_client_set_post_field(client, pubBuf, strlen(pubBuf));
                    esp_err_t ee = esp_http_client_perform(client);
                    if (ee == ESP_OK) {
                        ESP_LOGI(TAG, "Status = %d, content_length = %d",
                                esp_http_client_get_status_code(client),
                                esp_http_client_get_content_length(client));
                        xQueueReceive(hteQueue, &hte, (TickType_t) 0);
                    }
                    esp_http_client_cleanup(client);
                }
            }
            if(wiFiStationStatus == 1) {
                esp_err_t ee = esp_wifi_connect();
                ESP_LOGI(TAG, "esp_wifi_connect 0x%X", ee);
                if(ee == ESP_OK) {
                    wiFiStationStatus = 2;
                }
            }
        }
    }
}

esp_err_t commsPublishData(struct HbbTelemetryEntry * pHte) {
    return xQueueSendToBack(hteQueue, pHte, 2000 / portTICK_PERIOD_MS);
}

esp_err_t commsPublishMessage(const uint8_t iMsg) {
    struct timeval tv;
    struct timezone *tz;
    gettimeofday(&tv, &tz);
    struct HbbTelemetryEntry hte = {
        .flags = HTE_IS_A_MESSAGE,
        .ts = tv.tv_sec,
        .iMsg = iMsg
    };
    hte.ts *= 1000;
    hte.ts += tv.tv_usec / 1000;
    return commsPublishData(&hte);
}
